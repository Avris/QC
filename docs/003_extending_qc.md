## Extending QC ##

### Contribution ###

QC is open source, available on [Gitlab](https://gitlab.com/Avris/QC). Feel free to check out the code,
fork, sumbit pull requests, whatever. I'd appreciate!

### Creating libraries for QC ###

QC is written in PHP, and the libraries have to be as well. Library is just a set of functions that extend
QC's default function set. Registering them is easy:

    $qc = new QC()
    $qc->registerFunction('☀', 'MyNamespace\Functions\Sun');
    $qc->registerFunction('★', 'MyNamespace\Functions\Star');
    $qc->registerFunction('❤', 'MyNamespace\Functions\Heart');
    return $qc->run('"foo"5I2★/❤', $input, $asideOutput);

Those classes must extend `AbstractFunction`, i.e. define their arity, some description string,
and what they actually do. They can do anything. They have access to the stack, all the variables,
and the aside output.

Just take a look at what's in `Avris\QC\Token\Func` namespace, and do something similar.

### Custom AsideOutput ###

Just implement `AsideOutputInterface` with whatever you need it to do. It's not hard, really.
