## Reference ##

### Literals ###

| Literal | Description |
| --- | --- |
| `·` | null |
| `-12` | integer |
| `8. / 8.15 / .8` | float |
| `"13" / "a"` | string |
| `[1 2 abc ["foo bar"]]` | array |
| `<$pattern:$modifiers>` | Regular expression (matching) |
| `<$pattern:$modifiers;$replacement>` | Regular expression (replacing) |

### Controls ###

| Control | Description |
| --- | --- |
| `{$condition:$block}` | while ($condition) { $block } |
| `$condition¿$ifBlock:$elseBlock?` | if ($condition) { $ifBlock } else { $elseBlock } |
| `$iterator↪$block↩` | for (1..$iterator) { $block }<br/>foreach ($iterator) { $block } if $iterator is an array |
| `($name$arity:$block)` | function $name ($arity) { $block }<br/>Arity means number of parameters it takes from the stack |
| `(⪑$name$arity:$block)` | Providing ⪑-function with an array as a parameter will apply the function to all elements of that array<br/>Only works for arity 1 and 2 |
| `#comment` | Comment |
| `@$input => $expectedOutput` | Test case |
| `$$filename` | Include file $filename |

### Default variables ###

| Default variable | Description |
| --- | --- |
| `I` | $input |
| `C` | $code |
| `Z` | 0 |
| `J` | 1 |
| `T` | 10 |
| `E` | "" |
| `S` | " " |
| `K` | "," |
| `D` | "." |
| `A` | [] |
| `L` | "abcdefghijklmnopqrstuvwxyz" |
| `U` | "ABCDEFGHIJKLMNOPQRSTUVWXYZ" |
| `N` | "0123456789" |

### Constants ###

| Constant |
| --- |
| `∞` |
| `π` |
| `ℯ` |

### Functions ###

| Function | Class | Arity | Description |
| --- | --- | --- | --- |
| `=` | Assign | 2 | $a = $b |
| `¡` | PrintFunc | 1 | Prints $a |
| `!` | PrintNewLine | 1 | Prints $a and a new line |
| `↟` | Pop | 1 | Pops one element from the stack |
| `▲` | Ternary | 3 | $a ? $b : $c<br/>All the expressions get executed. If you don't want that, use ¿:? |
| `⇓` | Value | 1 | Returns the value of $a |
| `☉` | ArrayToVars | 1 | Iterates over $a (or I if stack is empty) and sets its values as variables a, b, c, ... consecutively |
| `ƶ` | Sleep | 1 | Sleeps $a seconds |
| `+` | Math\Plus | 2 | $a + $b |
| `-` | Math\Minus | 2 | $a - $b |
| `*` | Math\Times | 2 | $a * $b<br/>Or, if one argument is a string and the other is numeric, repeats the string given number of times |
| `/` | Math\Divide | 2 | $a/$b |
| `%` | Math\Modulo | 2 | $a % $b |
| `^` | Math\Power | 2 | $a to the power of $b |
| `√` | Math\Sqrt | 1 | Square root of $a |
| `㏒` | Math\Log | 2 | Logarithm of a $a with respect to base $b |
| `‡` | Math\Increment | 1 | $a++ |
| `⸗` | Math\Decrement | 1 | $a-- |
| `⩲` | Math\Add | 2 | $a += $b |
| `±` | Math\Negate | 1 | -$a |
| `‖` | Math\Abs | 1 | Absolute value of $a |
| `⌋` | Math\Floor | 1 | Round $a down |
| `⌉` | Math\Ceil | 1 | Round $a up |
| `≊` | Math\Round | 1 | Round $a |
| `‼` | Math\Factorial | 1 | $a factorial |
| `⋇` | Math\IsNumeric | 1 | Is $a numeric? |
| `℥` | Math\Random | 2 | Generates a random integer between $a and $b |
| `⇅` | Math\Base | 3 | Converts $a in base $b to base $c |
| `⩥` | Math\LimitRange | 3 | If $a is outside of range ($b, $c), it gets moved to its boundaries |
| `⊿` | Math\Hypotenuse | 2 | Hypotenuse of triangle with altitudes $a and $b |
| `∡` | Math\DegToRad | 1 | Convert $a degrees to radians |
| `⦛` | Math\RadToDeg | 1 | Convert $a radians to degrees |
| `⊾` | Math\Trig | 2 | Calculates $a trigonometrical function of value $b |
| `≟` | Compare\Equals | 2 | $a == $b |
| `≠` | Compare\NotEquals | 2 | $a != $b |
| `>` | Compare\Greater | 2 | $a > $b |
| `≥` | Compare\GreaterEqual | 2 | $a >= $b |
| `<` | Compare\Less | 2 | $a < $b |
| `≤` | Compare\LessEqual | 2 | $a <= $b |
| `∨` | Logic\LogicalOr | 2 | $a or $b |
| `∧` | Logic\LogicalAnd | 2 | $a and $b |
| `⊻` | Logic\LogicalXor | 2 | $a xor $b |
| `~` | Logic\LogicalNot | 1 | Not $a |
| `↹` | StringArray\Length | 1 | Length of string or size of array |
| `↓` | StringArray\PushTop | 2 | Put $b at the end of array $a |
| `↑` | StringArray\PushBottom | 2 | Put $b at the beginning of array $a |
| `⊕` | StringArray\Sum | 1 | Sum of array's elements |
| `⊗` | StringArray\Product | 1 | Product of array's elements |
| `⥍` | StringArray\Explode | 2 | Splits array $a with divider $b |
| `⥋` | StringArray\Implode | 2 | Joins array $a using string $b as a glue |
| `⥏` | StringArray\StringToArray | 1 | Splits array $a letter by letter |
| `⥎` | StringArray\ArrayToString | 1 | Joins array $a using string "" as a glue |
| `⥬` | StringArray\CharToAscii | 1 | Returns ASCII value of a character $a |
| `⥪` | StringArray\AsciiToChar | 1 | Returns character coresponding to given ASCII value |
| `ₓ` | StringArray\Range | 2 | Generate a range of integers between $a and $b |
| `₁` | StringArray\RangeOne | 1 | Generate a range of integers between 1 and $a |
| `₀` | StringArray\RangeZero | 1 | Generate a range of integers between 0 and $a |
| `ᴙ` | StringArray\Reverse | 1 | Reverses an array |
| `☌` | StringArray\FindIndex | 2 | $a[$b] or null |
| `☍` | StringArray\FindSubrange | 3 | Subarray of $a starting with $b (negative $b means counting from the end) of length $c |
| `⌊` | StringArray\Min | 1 | Minimum of array's elements |
| `⌈` | StringArray\Max | 1 | Maximum of array's elements |
| `∈` | StringArray\InArray | 2 | Checks if $a is an element of array $b |
| `Φ` | StringArray\FindPosition | 2 | Finds the position of the first occurance of $a in string/array $b (or -1 if not found) |
| `≈` | StringArray\AlmostEqual | 2 | Checks if string $a matches regex $b (commutative)<br/>Sets the R variable to found matches |
| `≉` | StringArray\NotAlmostEqual | 2 | Checks if string $a does not match regex $b (commutative)<br/>Sets the R variable to found matches |
| `⥵` | StringArray\Transform | 2 | Transforms string $a according to regex $b (commutative) |

