## Getting started ##

Writing the QC interpreter was really fun! But writing its documentation is not gonna be so, I suppose...
And hardly anybody is gonna read it anyway... So please, don't blame me for not being too thorough with it.
I'll try to be concise here (nomen omen), but to still cover everything important.

QC's interpreter is written in PHP. Don't ask why. Please, don't.

### Getting QC ###

Composer is a powerful package manager for PHP. It takes care of all the dependencies and stuff.
To install it, simply run this command in your console.

    curl -sS https://getcomposer.org/installer | php

Now you should have a `composer.phar` file. Run it with such parameters:

    php composer.phar require avris/qc

And voilà! QC is ready to use. Btw, if you want to use some developer tools on it, install it with the flag `--dev`.

### How it works ###

Well... It's all about the **[stack](https://en.wikipedia.org/wiki/Stack_%28abstract_data_type%29)**.
If you're still reading at this point, you're probably one of those people,
who already know, what stack is. And the [Reverse Polish notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation),
of course.

We're using both.

Every variable and literal (string, number, regex), when it's read by the interpreter, gets put on stack.
Every function, on the other hand, pops some given **(fixed!)** number of elements from the stack and executes
some operation on them, then puting its result back on the stack

For instance: what we'd normally write down as `(2 + 4) * 3`, in QC is: `2 4+3*`. We put `2` on stack, then `4`,
and then we execute `+` function. It takes two arguments from the stack, adds them together, and puts the result,
`6` back. We put `3` on top of that, and then `*` function comes in, taking those two values off, but giving `18`
back. That's what we end up with at the top of the stack, so that's our result.

Note how `2` and `4` are separated by a space. Otherwise it would be understood as one number, `24`.
The space wouldn't be necessary if, for instance, we had a variable that's equal to 4. Let's say `I`.
Then our code becomes just `2I+3*`.

Btw, `I` just happens to be a predefined variable equal to whatever you provided your program with as input data.
We have a couple of predefined variables, all listed at the end of this documentation, in [Reference](#reference) section.
Actually, a lot of other stuff is listed there. Take a look, really. I'm not gonna explain everything here.

All the english letters, upper- and lowercase, are QC's variables. For the functions, we use other characters:
either normal stuff like `+` or `%`, or something more fancy, like `☉`, `√`, `⩲`. Get used to it.

I know, there could be much trouble regarding Unicode usage. Not every console supports it, some websites might
break it, blah blah... Oh c'mon, Unicode isn't magic, it isn't some freaky non-standard conceit!
It's not my fault, if some tools you might be using don't support the XXI century.

### Input parsing ###

When providing some input straight to QC as a PHP value, everything is fine, it knows exaclty what you meant.
However, in the console or online interpreter, things get a bit more complicated. Writing `2` doesn't tell QC,
if you mean a number 2, or a string with one character "2". So, the deal is:

 * if it's in quotes, `""`, it's a string, no matter what,
 * if it's numeric, we parse it as a number; having a dot, makes it a float: `7.` means `7.0`, `.7` means `0.7`;
and `7` means `7`, the integer
 * if it's in square brackets, `[]`, it's an array, recurreccy allowed, e.g.: `[1 2 [a 3]]`.

### Aside output ###

Apart from returning a single value, your QC program can produce also other type of data:

 * standard output, printed out with `!` and `¡` functions,
 * debug info: a step-by-step list of which token is handled now and how does the stack look at that point,
 * list of test cases and their results: obviously only in testsuite mode.

Those types of data are handled by the `AsideOutput` object that you pass to `$qc->run()`. Of course,
you can implement your own one, if you need to.