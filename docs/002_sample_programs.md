## Sample programs ##

Example is always the best way to learn something. Let's take a look at a couple of them.

### Hello World ###

    "Hello World!"

That's it. We put a string literal on the stack. Whatever is at the top of the stack at the end of the execution,
is our output. We could also print it on the screen, instead of just returning it, if we use the `!` function:

    "Hello World!"!

### Champernowne ###

The [Champernowne constant](https://en.wikipedia.org/wiki/Champernowne_constant) is equal to
`0.1234567891011121314...`. And the program below finds the first appearance of a given number (`I` variable)
in its decimal expansion:

    IT+₁E⥋IΦ‡

Some test cases:

    @20 => 30
    @333 => 56
    @0 => 11     #because the zero before the dot doesn't count
    @2930 => 48

So what happens there in the code? First, we add `I` to `T`: `IT+`. `I` is our input, let's say 17,
and `T` is predefined variable equal to ten. So now we have 27 on the stack. The `₁` function creates
a range from 1 to its argument. So now our stack top is an array: `[1 2 3 ... 26 27]`. The whole
"adding ten" thing is necessary for the `0` case.

Function `ab⥋` joins an array `a` using string `b` as a glue. Here, our glue is `E` -- a predefined variable
containing an empty string. So after joining, our stack top is a string: `1234...2627`.

`abΦ` finds the position of the first occurance of `b` in the string/array `a`. In our case: the position
of `I=17` in string `1234...2627`. Which is `23`, because `Φ` starts indexing at `0`. To get the 1-based response,
we have to increment. `23‡` is `24` -- and that's our answer.

### Random walk ###

Our input is `[m n]`, and our output should be something like that:

                       .
                         .
                       .
                       .
                      .
                    .
                     .
                    .
                      .
                    .

We make `m` steps, each lasting `n` seconds, and we randomly move to the left or to the right
either by -2, -1, 0, 1 or 2 fields. Going below 0 or above 39 is not allowed.

So, let's look at the code:

    ☉X20=a↪XX-2 2℥+Z39⩥=XSX*D+!bƶ↩·

`☉` is a shortcut function that takes an array (from the stack or, if it's empty, from the input), and assigns its
values to variables `a`, `b`, `c`... and so on. So in our case, assuming we inserted the input `[10 1]`,
the `☉` function automatically assinges two variables: `a=10` and `b=1`.

`X20=` -- variable assignment; `X` is our current position from the left edge, and is equal to `20`.

`a↪...↩` -- repeat `...` for `a` times. Quite staight forward. We make `a=10` steps.

`-2 2℥` -- random integer between `-2` and `2`.

`X...+` -- add the random number to our `X` variable.

`abc⩥` -- make sure `a` is between `b` and `c`. Here, we make sure that `X` is not less than `Z=0` and
not greater than `39`.

`X...=` -- we assign the result back to `X`.

`SX*D+` -- multiply `S=" "` times `X` (so we get `X` consecutive spaces), and then add `D="."`.
"Add" means "concatenate", if we're talking about string, of course. In QC one function can do different
things depending on the type of its arguments.

Now, we just print (`!`) our string and wait (`ƶ`) for `b` seconds. After the loop we put a null (`·`)
on the stack, to make sure the program doesn't return anything.

### Factorial ###

    (⪑☯1:a¿aa1-☯*:1?)I☯

If you read the example in [Readme](#readme-sample-code), you should already be able to understand this code.
What's different here, is that we use recursive calls. Easy thing, right?

Oh, and by the way: we don't have to calculate factorial like that. There's an in-build function for it:

    I‼
