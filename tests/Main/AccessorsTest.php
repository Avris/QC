<?php
namespace Avris\QC;

use Avris\QC\Exception\ErrorHandler;
use Avris\QC\Exception\ParseException;
use Avris\QC\Exception\RuntimeException;
use Avris\QC\Token\Control\FunctionControl;
use Avris\QC\Token\Control\WhileControl;
use Avris\QC\Token\Func\ArrayToVars;
use Avris\QC\Token\Func\FunctionPromise;
use Avris\QC\Token\Func\QCFunction;
use Avris\QC\Token\Literal;
use Avris\QC\Token\Regex;
use Avris\QC\Token\Variable;

class AccessorsTest extends \PHPUnit_Framework_TestCase
{
    public function testVariable()
    {
        $var = new Variable('A', 7);
        $this->assertEquals('A', $var->getName());
        $var->setName('B');
        $this->assertEquals('B', $var->getName());
        $this->assertEquals('B', $var->getDump());
        $this->assertEquals(7, $var->jsonSerialize());
    }

    public function testAbstractControl()
    {
        $handler = new ErrorHandler();

        // just to cover empty abstract overwrite
        $control = new WhileControl([], [], new Tokenery());
        $control->run([]);

        $this->assertEquals('', $control->getDescription());

        $control = new FunctionControl('☯', false, 1, [], new Tokenery());
        $this->assertEquals('', $control->getDescription());
        $control->setInternalTokenery(new Tokenery());

        $control = new QCFunction('☯', false, 1, [], new Tokenery());
        $this->assertEquals('', $control->getDescription());

        $function = new ArrayToVars('', new Tokenery());
        $function->run([]);
        $this->assertFalse($function->pushesResult());

        $handler->destruct();
    }

    public function testAbstractValue()
    {
        $a = new Variable('a', 1);
        $this->assertTrue($a->isNumber());
        $this->assertFalse($a->isString());
        $this->assertFalse($a->isArray());

        $b = new Variable('b', '1');
        $this->assertFalse($b->isNumber());
        $this->assertTrue($b->isString());
        $this->assertFalse($b->isArray());

        $c = new Variable('c', [1]);
        $this->assertFalse($c->isNumber());
        $this->assertFalse($c->isString());
        $this->assertTrue($c->isArray());
    }

    /**
     * @expectedException \Avris\QC\Exception\InvalidArgumentException
     */
    public function testLiteral()
    {
        new Literal('foobar');
    }

    public function testRegex()
    {
        $regex = new Regex('a<b>c:d;e', 'u', true, 'xxx');
        $this->assertEquals('<a<b\>c\:d\;e:u;xxx>', $regex->getDump());
    }

    public function testFunctionPromise()
    {
        $promise = new FunctionPromise('☯', new Tokenery());
        $this->assertEquals(null, $promise->getDescription());
        $this->assertEquals(null, $promise->getArity());
        $this->assertEquals(null, $promise->run([]));
        try {
            $promise->execute(new Stack(), function(){}, function(){});
            $this->fail('Exception expected');
        } catch (ParseException $e) {
            $this->assertEquals('Unrecognised function "☯"', $e->getMessage());
        }
    }

    public function testErrorHandler()
    {
        $handler = new ErrorHandler();
        try {
            str_repeat('a', -5);
            $this->fail('Exception expected');
        } catch (RuntimeException $e) {}

        $handler->destruct();
        try {
            $handler->destruct();
            $this->fail('Exception expected');
        } catch (RuntimeException $e) {}
    }

}
