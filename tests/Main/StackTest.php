<?php
namespace Avris\QC;

use Avris\QC\Token\Literal;

class StackTest extends \PHPUnit_Framework_TestCase
{

    public function testStack()
    {
        $stack = new Stack();
            
        $this->assertEquals(0, $stack->getSize());
        $stack->push(new Literal(Parser::TOKEN_NUMBER, 1));
        $this->assertEquals(1, $stack->getSize());
        $stack->push(new Literal(Parser::TOKEN_NUMBER, 2));
        $this->assertEquals(2, $stack->getSize());

        $elements = $stack->getElements();
        $this->assertCount(2, $elements);
        $this->assertInstanceOf('\Avris\QC\Token\Literal', $elements[0]);
        $this->assertEquals('[1, 2]', (string)$stack);

        $element = $stack->pop();
        $this->assertInstanceOf('\Avris\QC\Token\Literal', $element);
        $this->assertEquals(2, $element->getValue());
        $this->assertEquals(1, $stack->getSize());

        $stack->push(new Literal(Parser::TOKEN_NUMBER, 3));
        $this->assertEquals(2, $stack->getSize());

        $this->assertEquals(2, $stack->getSize());
        $this->assertEquals('[1, 3]', (string)$stack);

        $element = $stack->pop();
        $this->assertInstanceOf('\Avris\QC\Token\Literal', $element);
        $this->assertEquals(3, $element->getValue());
        $this->assertEquals(1, $stack->getSize());

        $element = $stack->pop();
        $this->assertInstanceOf('\Avris\QC\Token\Literal', $element);
        $this->assertEquals(1, $element->getValue());
        $this->assertEquals(0, $stack->getSize());

        $element = $stack->pop();
        $this->assertInstanceOf('\Avris\QC\Token\EmptyToken', $element);
        $this->assertEquals(0, $stack->getSize());
        $this->assertEquals('[]', (string)$stack);

        $element->execute($stack, function(){}, function(){});
        $this->assertEquals('[]', (string)$stack);

        $stack->push(new Literal(Parser::TOKEN_NUMBER, 4));
        $this->assertEquals(1, $stack->getSize());
        $this->assertEquals('[4]', (string)$stack);
    }

}
