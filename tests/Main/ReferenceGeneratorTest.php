<?php
namespace Avris\QC;

class ReferenceGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testGenerateDocs()
    {
        $rg = new ReferenceGenerator();
        $reference = $rg->generateReference();

        $this->assertEquals(['literals', 'controls', 'defaultVariables', 'constants', 'functions'], array_keys($reference));
        $this->assertEquals(['name', 'insert', 'class', 'arity', 'description'], array_keys($reference['functions'][0]));
    }

    public function testGenerateMarkdown()
    {
        $rg = new ReferenceGenerator();
        $md = $rg->generateMarkdown();

        $this->assertContains('### Literals ###', $md);
        $this->assertContains('### Controls ###', $md);
        $this->assertContains('### Default variables ###', $md);
        $this->assertContains('### Functions ###', $md);
    }

    public function testGenerateHtml()
    {
        $rg = new ReferenceGenerator();
        $reference = json_encode($rg->generateReference());
        $html = $rg->generateHtml(json_decode($reference, true));
        $html2 = $rg->generateHtml();

        $this->assertEquals($html, $html2);

        $this->assertContains('data-section="Literals"', $html);
        $this->assertContains('data-section="Controls"', $html);
        $this->assertContains('data-section="Default variables"', $html);
        $this->assertContains('data-section="Functions"', $html);
    }
}
