<?php
namespace Avris\QC;

use Avris\QC\Exception\QCException;
use Avris\QC\Token\Literal;

class QCTest extends \PHPUnit_Framework_TestCase
{
    /** @var QC */
    protected $qc;

    protected function setUp()
    {
        $this->qc = new QC();
    }

    /**
     * @dataProvider runProvider
     */
    public function testRun($code, $input, $output)
    {
        ob_start();
        $this->assertEquals($output, $this->qc->run($code, Helper::parseInput($input)));
        ob_get_clean();
    }

    public function runProvider()
    {
        return array_merge(
            $this->runProviderGeneral(),
            $this->runProviderControl(),
            $this->runProviderStringArray(),
            $this->runProviderCompare(),
            $this->runProviderLogic()
        );
    }

    protected function runProviderGeneral()
    {
        return [
            ['3 15 2 3+-*2/4%', '', 3],
            ['N5=NI+', '2', 7],
            ['"lol test"" hihi"+', '', 'lol test hihi'],
            ['4"t"+I+', '5', '4t5'],
            ['"test "2 3*+', '', 'test 6'],
            ['-7. I%', '3', -1],
            ['I‡I+', 5, 12],
            ['I⇓‡I+', 5, 11],
            ['5I^', 2, 25],
            ['I√', 2, sqrt(2)],
            ['I‖', '[-4 0 6]', [4, 0, 6]],
            ['7.9⌋', '', 7],
            ['6.2⌉', '', 7],
            ['6.2≊', '', 6],
            ['6.2≊', '', 6],
            ['3π*', '', 3 * M_PI],
            ['5.7 1.3%', '', 0.5],
            ['Z2⩲Z', '', 2],
            ['<l(a|o);r\1>I⥵', 'lalola', 'rarora'],
            ['7 10 2⇅', '', '111'],
            ['"110" 2 10⇅', '', 6],
            ['I₀10 2⇅', '8', ["0", "1", "10", "11", "100", "101", "110", "111", "1000"]],
            ['3 4⊿', '', 5],
            ['☉c', '[1 2 3]', 3],
            ['A5↓A6↓☉b', '[1 2 3]', 6],
            ['I⌊', '[1 2 3 -9]', -9],
            ['I⌈', '[1 2 3 -9]', 3],
            ['I⋇', '[1 a "4"]', [1, 0, 1]],
            ['1 1℥', '', 1],
            ['I2㏒', '16', 4],
            ['360∡', '', 2 * M_PI],
            ['1⦛', '', rad2deg(1)],
            ['"sin"30∡⊾', '', 0.5],
            ['IT+₁E⥋IΦ‡', '20', 30],
            ['I5Φ', '[9 5 4]', 1],
            ['I5Φ', '[9 3 4]', -1],
            ['Iƶ', '0', null],
            ['I 3 7⩥', '-1', 3],
            ['I 3 7⩥', '3', 3],
            ['I 3 7⩥', '4', 4],
            ['I 3 7⩥', '8', 7],
        ];
    }

    protected function runProviderControl()
    {
        return [
            ['3I2-¿I‡2+:4?I*', '2', 8],
            ['3I2-¿I‡2+:4?I*', '3', 24],
            ['II¿I‡?', '3', 4],
            ['II¿I‡?', '0', 0],
            ['II¿:I‡?', '2', 2],
            ['II¿:I‡?', '0', 1],
            ['I⸗±', '5', -4],
            ['{I⸗:k‡}k', '3', 2],
            ['5 4↟', '', 5],
            ['3!', '', 3],
            ['3¡', '', 3],
            ['II"a"▲', '4', 4],
            ['II"a"▲', '0', 'a'],
            ['(☯1:{Aa⇓↓a1>:aa2%a3*1+a2/▲=}A↹)I☯A', 5, []], //scope clean
            ['(☯1:{Aa⇓↓a1>:aa2%a3*1+a2/▲=}A↹)I☯', 5, 6],
            ['(⪑☯1:a2%¿a2*:a2/?)I☯', 5, 10],
            ['(⪑☯1:a2%¿a2*:a2/?)I☯', '[5 6]', [10, 3]],
            ['(☯2:ab*)I5☯', '3', 15],
            ['(⪑☯2:ab*)I5☯', '3', 15],
            ['(⪑☯2:ab*)I5☯', '[3 4]', [15, 20]],
            ['I↪Aa↑↩A', '[1 2 6]', [6, 2, 1]],
            ['I↪Aa↑↩A', '-3', [-3, -2, -1, 0, 1]],
            ['(☯2:a↪Aa↑bb2*=↩)5I☯', '3', [10, 20, 40]],
            ['I‼', '5', 120],
            ['(⪑☯1:a0≟¿1:aa1-☯*?)I☯', '5', 120],
            // nesting
            ['I¿I‡¿"X":"Y"?:"Z"?', '-1', 'Y'],
            ['I¿I‡¿"X":"Y"?:"Z"?', '0', 'Z'],
            ['I¿I‡¿"X":"Y"?:"Z"?', '1', 'X'],
            ['I¿"A":I‡¿"B":"C"??', '-1', 'A'],
            ['I¿"A":I‡¿"B":"C"??', '0', 'B'],
            ['I¿"A":I‡¿"B":"C"??', '1', 'A'],
            ['X3={X⸗:Y4={Y⸗:AXY*↓}}A', '', [
                new Literal(Parser::TOKEN_NUMBER, 6),
                new Literal(Parser::TOKEN_NUMBER, 4),
                new Literal(Parser::TOKEN_NUMBER, 2),
                new Literal(Parser::TOKEN_NUMBER, 3),
                new Literal(Parser::TOKEN_NUMBER, 2),
                new Literal(Parser::TOKEN_NUMBER, 1),
            ]],
            ['I↪ba=↪Aab*↓↩↩A⊕', '3', 25],
        ];
    }

    protected function runProviderStringArray()
    {
        return [
            ['"aaa"↹', '', 3],
            ['"4"↹', '', 1],
            ['4↹', '', 1],
            ['4I*', '4', 16],
            ['4I*', '"a"', 'aaaa'],
            ['I4*', '"a"', 'aaaa'],
            ['4I*', '"4"', '4444'],
            ['[1 2 3][4 5]+', '', [5, 7, 3]],
            ['[4 5][1 2 3]+', '', [5, 7, 3]],
            ['1[1 2 3]+', '', [2, 3, 4]],
            ['[1 2 3]1+', '', [2, 3, 4]],
            ['[1 2][3 4 5]-', '', [-2, -2, -5]],
            ['[1 2][3 4 5]*', '', [3, 8, 5]],
            ['[1 2][3 4 5]/', '', [1/3, 2/4, 1/5]],
            ['[5 2 3 8]2%', '', [1, 0, 1, 0]],
            ['[0 2 -3 8]‡', '', [1, 3, -2, 9]],
            ['[0 2 -3 8]⸗', '', [-1, 1, -4, 7]],
            ['[0 2 -3 8]±', '', [0, -2, 3, -8]],
            ['I↹', '[3 a "siema"]', 3],
            ['A[1 2]=3↓', '', [1, 2, new Literal(Parser::TOKEN_NUMBER, 3)]],
            ['A[1 2]=3↑', '', [3, 1, 2]],
            ['[-1 2 3 a]⊕', '', 4],
            ['[-1 2 3]⊕', '', 4],
            ['[-1 2 3]⊗', '', -6],
            ['"1 2 3 a"S⥍', '', ['1', '2', '3', 'a']],
            ['[1 2 3 a]S⥋', '', '1 2 3 a'],
            ['5Iₓ', 8, [5, 6, 7, 8]],
            ['5Iₓ', 3, [5, 4, 3]],
            ['I₁', 3, [1, 2, 3]],
            ['I₀', -3, [0, -1, -2, -3]],
            ['I₁ᴙ', '6', [6, 5, 4, 3, 2, 1]],
            ['[a b c]I☌', 2, 'c'],
            ['[a b c]I☌', 3, null],
            ['[a b c]-1 6☍', '', ['c']],
            ['1[3 2 a]∈', '', 0],
            ['1[3 2 a 1]∈', '', 1],
            ['<a+>I≈', 'test', 0],
            ['<a+>I≈', 'teaaast', 1],
            ['<a+>I≉', 'teaaast', 0],
            ['A<a+:U>=AI≈R', 'teaaast', ['a']],
            ['<a+;xy>I⥵', 'teaaast', 'texyst'],
            ['I⥎', '[a b c d]', 'abcd'],
            ['I⥏', 'abcd', ['a', 'b', 'c', 'd']],
            ['I⥏', 'abcd', ['a', 'b', 'c', 'd']],
            ['I⥬', 'a', 97],
            ['I⥬', 'avris', [97, 118, 114, 105, 115]],
            ['I⥪', 97, 'a'],
            ['I⥪', '[97 118 114 105 115]', 'avris'],
        ];
    }

    protected function runProviderCompare()
    {
        return [
            ['I5≟', '5', 1],
            ['I5≟', '4', 0],

            ['I5≠', '5', 0],
            ['I5≠', '4', 1],

            ['I5>', '6', 1],
            ['I5>', '5', 0],
            ['I5>', '4', 0],

            ['I5≥', '6', 1],
            ['I5≥', '5', 1],
            ['I5≥', '4', 0],

            ['I5<', '6', 0],
            ['I5<', '5', 0],
            ['I5<', '4', 1],

            ['I5≤', '6', 0],
            ['I5≤', '5', 1],
            ['I5≤', '4', 1],

            ['I5≥', '[5 1 -3 18]', [1, 0, 0, 1]],
        ];
    }

    protected function runProviderLogic()
    {
        return [
            ['I1∨', '4', 1],
            ['I1∨', '0', 1],
            ['I0∨', '4', 1],
            ['I0∨', '0', 0],

            ['I1∧', '4', 1],
            ['I1∧', '0', 0],
            ['I0∧', '4', 0],
            ['I0∧', '0', 0],

            ['I1⊻', '4', 0],
            ['I1⊻', '0', 1],
            ['I0⊻', '4', 1],
            ['I0⊻', '0', 0],

            ['I~', '4', 0],
            ['I~', '0', 1],

            ['I0∨', '[5 0 1]', [1, 0, 1]],
            ['I[1 1 0]∧', '[5 0 1]', [1, 0, 0]],
            ['I~', '[5 0 1]', [0, 1, 0]],
        ];
    }

    /**
     * @dataProvider runErrorProvider
     */
    public function testRunError($code, $input, $message)
    {
        try {
            $this->qc->run($code, Helper::parseInput($input));
            $this->fail('An exception was expected to be thrown');
        } catch (QCException $e) {
            $this->assertEquals($message, $e->getMessage());
        }
    }

    public function runErrorProvider()
    {
        return [
            ['3+', '', 'Function "+" requires 2 parameters, 1 given'],
            ['xyz"', '', 'Unable to parse code: "'],
            ['2I=', '4', 'Left argument of "=" must be a variable'],
            ['3I/', '0', 'Division by zero'],
            ['3I%', '0', 'Division by zero'],
            ['3I%', '0', 'Division by zero'],
            ['"a""a"*', '', 'Cannot multiply string by string'],
            ['1[]⥋', '', 'Argument 1 of "⥋" has to be an array'],
            ['1[]↑', '', 'Argument 1 of "↑" has to be an array'],
            ['1[]↓', '', 'Argument 1 of "↓" has to be an array'],
            ['1⊕', '', 'The argument of "⊕" has to be an array'],
            ['1⊗', '', 'The argument of "⊗" has to be an array'],
            ['1 2☌', '', 'Argument 1 of "☌" has to be an array'],
            ['1 2 3☍', '', 'Argument 1 of "☍" has to be an array'],
            ['Zᴙ', '', 'The argument of "ᴙ" has to be an array'],
            ['AB≈', '', 'Exactly one argument of Regex related functions has to be a regex'],
            ['1⌊', '', 'The argument of "⌊" has to be an array'],
            ['1⌈', '', 'The argument of "⌈" has to be an array'],
            ['1☉', '', 'The argument of "☉" has to be an array'],
            ['"grr"30∡⊾', '', '"grr" is not a valid trigonometrical function'],
            ['1 2∈', '', 'Argument 2 of "∈" has to be an array'],
            ['(⪑☯3:)', '', 'User function with ⪑ option has to have arity of 1 or 2'],
            ['1 2⩲', '', 'Argument 1 of ⩲ has to be a variable'],
            ['5⥎', '', 'Argument 1 of "⥎" has to be an array'],
        ];
    }
}
