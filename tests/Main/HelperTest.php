<?php
namespace Avris\QC;

use Avris\QC\Exception\RuntimeException;

class HelperTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider dumpValueProvider
     */
    public function testDumpValue($input, $output)
    {
        $this->assertEquals($output, Helper::dumpValue($input, 0));
    }

    public function testDumpValueTrim()
    {
        $this->assertEquals('"abcdefghi…"', Helper::dumpValue('abcdefghijklmnopqrstuvwxyz'));
        $this->assertEquals('["1a" "2b"…]', Helper::dumpValue(['1a', '2b', [32, "4"], 5, [6, [7]]]));
    }

    public function dumpValueProvider()
    {
        return [
            [null, '·'],
            [7, '7'],
            [-7.0, '-7.'],
            [-7.25, '-7.25'],
            ['-7.25', '"-7.25"'],
            ['foo', '"foo"'],
            [['1a', '2b', [32, "4"], 5, [6, [7]]], '["1a" "2b" [32 "4"] 5 [6 [7]]]']
        ];
    }

    /**
     * @dataProvider parseInputProvider
     */
    public function testParseInput($input, $output)
    {
        $this->assertSame($output, Helper::parseInput($input));
    }

    public function parseInputProvider()
    {
        return [
            ['·', null],
            ['', ''],
            ['"f\"oo"', 'f"oo'],
            ['7', 7],
            ['7.', 7.0],
            ['"7"', '7'],
            ['-7.25', -7.25],
            ['[1a 2b [32 "4"] 5 [6 [7]]]', ['1a', '2b', [32, "4"], 5, [6, [7]]]],
            ['[a a [', '[a a ['],
            ['[1a 2b [32 "4"] "5 \"x" [6 ["7 a"]]]', ['1a', '2b', [32, "4"], '5 "x', [6, ['7 a']]]],
        ];
    }

    public function testGenerateTestsuite()
    {
        $this->assertEquals(
            "\n@1 => ·\n@2 => \"-3\"\n@3 => -3\n",
            Helper::generateTestsuite([1 => null, 2 => '-3', 3 => -3])
        );
    }

    public function testIsHash()
    {
        $this->assertTrue(Helper::isHash(['foo' => 'bar']));
        $this->assertFalse(Helper::isHash(['foo', 'bar']));
    }

    public function testLoadSourceFromFile()
    {
        $code = Helper::loadSourceFromFile(__DIR__ . '/../Help/foo.qc');
        $this->assertEquals("(☯1:a2*)\n\nI☯", $code);

        try {
            $code = Helper::loadSourceFromFile(__DIR__ . '/../Help/baz.qc');
            $this->assertEquals("(☯1:a2*)\n\nI☯", $code);
            $this->fail('Exception expected');
        } catch (RuntimeException $e) {
            $this->assertContains('nonexistent.qc does not exist', $e->getMessage());
        }
    }
}
