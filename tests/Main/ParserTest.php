<?php
namespace Avris\QC;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    /** @var Parser */
    protected $qcParser;

    protected function setUp()
    {
        $this->qcParser = new Parser(new Tokenery());
    }

    /**
     * @dataProvider provider
     */
    public function testParse($code, $expectedTokens)
    {
        $tokens = $this->qcParser->parse($code);
        foreach ($tokens as $i => $token) {
            $this->assertInstanceOf('Avris\QC\Token\AbstractToken', $token);
            $this->assertEquals($expectedTokens[$i], (string)$token);
        }
    }

    public function provider()
    {
        return [
            ['12 1+2*', ['12', '1', '+', '2', '*']],
            ['12 1+2*3/', ['12', '1', '+', '2', '*', '3', '/']],
            ['3', ['3']],
            ['lol', ['l=·','o=·','l=·']],
            ['"lol"', ['"lol"']],
            ['"lol""ech test"+', ['"lol"','"ech test"','+']],
            ['"f\"esc\"b"', ['"f"esc"b"']],
        ];
    }
}
