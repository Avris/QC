<?php
namespace Avris\QC;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Exception\QCException;

class TokeneryTest extends \PHPUnit_Framework_TestCase
{
    /** @var Tokenery */
    protected $factory;

    public function setUp()
    {
        $this->factory = new Tokenery();
    }

    /**
     * @dataProvider exceptionProvider
     */
    public function testException($token, $value, $exceptionClass, $exceptionMessage)
    {
        try {
            $this->factory->buildToken($token, $value);
            $this->fail(sprintf('Exception "%s" with message "%s" expected', $exceptionClass, $exceptionMessage));
        } catch (QCException $e) {
            $this->assertInstanceOf('\Avris\QC\Exception\\' . $exceptionClass, $e);
            $this->assertEquals($exceptionMessage, $e->getMessage());
        }
    }

    public function exceptionProvider()
    {
        return [
            ['"', '', 'ParseException', 'Unrecognised token " ()'],
            [Parser::TOKEN_CONSTANT, 'notaconstant', 'ParseException', 'Unrecognised constant "notaconstant"'],
        ];
    }

    public function testVariables()
    {
        $a = $this->factory->getVariable('a');
        $this->assertInstanceOf('\Avris\QC\Token\Variable', $a);
        $this->assertEquals(null, $a->getValue());

        $this->factory->setVariable('a', 1);
        $this->assertEquals(1, $a->getValue());

        $this->assertEquals(['a' => $a], $this->factory->getVariables());
    }

    public function testRegisterFunction()
    {
        $qc = new QC();

        $this->registerFunctionException($qc, '+', '', 'Function "+" is already registered');
        $this->registerFunctionException($qc, '%%', '', 'Function name has to be one, non-reserved character');
        $this->registerFunctionException($qc, 'a', '', 'Function name has to be one, non-reserved character');
        $this->registerFunctionException($qc, '[', '', 'Function name has to be one, non-reserved character');

        $this->registerFunctionException($qc, '☯', 'foo', 'Class \foo does not exist');
        $this->registerFunctionException($qc, '☯', '\foo', 'Class \foo does not exist');
        $this->registerFunctionException($qc, '☯', '\stdClass', 'Class \stdClass is not an instance of \Avris\QC\Token\Func\AbstractFunction');

        $qc->registerFunction('☯', 'Avris\QC\Help\Collatz');

        $this->assertEquals(16, $qc->run('I☯', 5));
        $this->assertEquals(4, $qc->run('I☯', 8));
    }

    protected function registerFunctionException(QC $qc, $name, $class, $exception)
    {
        try {
            $qc->registerFunction($name, $class);
            $this->fail('Exception should be thrown');
        } catch (InvalidArgumentException $e) {
            $this->assertEquals($exception, $e->getMessage());
        }
    }
}
