<?php
namespace Avris\QC;

use AspectMock\Test as test;
use Avris\QC\Console\QCApplication;
use Avris\QC\Token\Variable;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Tester\CommandTester;

class ConsoleTest extends \PHPUnit_Framework_TestCase
{
    protected function tearDown()
    {
        test::clean();
    }

    /**
     * @dataProvider provider
     */
    public function testExecute($input, $output)
    {
        test::double('Avris\QC\Tokenery', ['getVariables' => [new Variable('I', null)]]);

        $application = new QCApplication();

        $this->assertEquals('qc', $application->getCommandName(new StringInput('')));

        $command = $application->find('qc');
        $commandTester = new CommandTester($command);
        $commandTester->execute($input);

        $this->assertEquals(
            ' QC version ' . QC::VERSION . ' ' .  $output,
            Helper::clearWhitespaces($commandTester->getDisplay())
        );
    }

    public function provider()
    {
        return [
            [
                [],
                'Usage: qc [options] [--] [<code>] [<input>] '.
                'Arguments: '.
                'code QC source code to execute '.
                'input Input data to operate on '.
                'Options: '.
                '-d, --debug Show debug data '.
                '-l, --debugLimit[=DEBUGLIMIT] Limit number of debug output lines [default: 100] ' .
                '-f, --file Treat "code" as a filename '.
                '-s, --suite Run for testsuites instead of input ' .
                '-h, --help Display this help message '.
                '-q, --quiet Do not output any message '.
                '-V, --version Display this application version '.
                '--ansi Force ANSI output --no-ansi Disable ANSI output '.
                '-n, --no-interaction Do not ask any interactive question '.
                '-v|vv|vvv, --verbose Increase the verbosity of messages: '.
                '1 for normal output, 2 for more verbose output and 3 for debug '
            ],
            [
                ['code' => '3 2+'],
                'Code size: 4 chars, 4 bytes '.
                'RESULT 5 '
            ],
            [
                ['code' => '3 I+', 'input' => 2, '--debug' => true],
                'Code size: 4 chars, 4 bytes '.
                '+-------+----------+-----------+ '.
                '| Token | Stack | Variables | '.
                '+-------+----------+-----------+ '.
                '| 3 | [3] | I=2 | '.
                '| I | [3, I=2] | I=2 | '.
                '| + | [5] | I=2 | '.
                '+-------+----------+-----------+ '.
                'RESULT 5 '
            ],
            [
                ['code' => '+'],
                'Code size: 1 chars, 1 bytes '.
                'ERROR Function "+" requires 2 parameters, 0 given '
            ],
            [
                [
                    'code' => '5I/' . Helper::generateTestsuite([5 => 1, 2 => 2.5, 1 => 5, 0 => null]),
                    '--suite' => true
                ],
                'Code size: 3 chars, 3 bytes '.
                '+-------+----------+----------+-------+ '.
                '| Input | Expected | Actual | '.
                '+-------+----------+----------+-------+ '.
                '| 5 | 1 | 1 | ✓ | '.
                '| 2 | 2.5 | 2.5 | ✓ | '.
                '| 1 | 5 | 5 | ✓ | '.
                '| 0 | · | Division by zero | '.
                '+-------+----------+----------+-------+ '.
                'FAILED! 3/4 ',
            ],
            [
                [
                    '--file' => true,
                    'code' => 'nonexistentfile.qc',
                ],
                'ERROR Source file '.realpath(__DIR__ . '/../../').'/nonexistentfile.qc does not exist ',
            ],
            [
                [
                    '--file' => true,
                    'code' => 'demo/math.qc',
                    'input' => 2
                ],
                'Code size: 3 chars, 3 bytes RESULT 2.5 ',
            ],
            [
                ['code' => 'I', 'input' => '5', '--suite' => true],
                'Code size: 1 chars, 1 bytes '.
                'ERROR Specified testsuite should be an array '
            ],
        ];
    }
}
