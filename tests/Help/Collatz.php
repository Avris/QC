<?php
namespace Avris\QC\Help;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Collatz extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return $a % 2 ? 3 * $a + 1 : $a / 2; });
    }

    public function getDescription()
    {
        return 'Returns x/2 if x is even, 3x+1 if x is odd';
    }
}
