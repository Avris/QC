<?php
namespace Avris\QC\AsideOutput;

use Symfony\Component\Console\Tests\Fixtures\DummyOutput;

class SymfonyOutputTest extends OutputTest
{
    /** @var DummyOutput */
    protected $sfoutput;

    public function init()
    {
        self::$expectedDebugOutput =
            '3 [3] '.
            'Debug output lines limit reached... '.
            '5 ';

        self::$expectedSuiteOutput =
            '1 5 5 ✓ '.
            '2 4 2.5 ✗ '.
            '0 8 Division by zero ';

        $this->sfoutput = new DummyOutput();
        $this->output = new SymfonyOutput($this->sfoutput, 2);
    }

    protected function fetch()
    {
        return $this->sfoutput->fetch();
    }
}
