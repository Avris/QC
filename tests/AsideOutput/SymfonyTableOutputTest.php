<?php
namespace Avris\QC\AsideOutput;

use Symfony\Component\Console\Tests\Fixtures\DummyOutput;

class SymfonyTableOutputTest extends OutputTest
{

    /** @var DummyOutput */
    protected $sfoutput;

    public function init()
    {
        self::$expectedDebugOutput =
            '5 '.
            '+-------------+-------+-----------+ '.
            '| Token | Stack | Variables | '.
            '+-------------+-------+-----------+ '.
            '| 3 | [3] | | '.
            '+-------------+-------+-----------+ '.
            '| Debug output lines limit reached... | '.
            '+-------------+-------+-----------+ ';

        self::$expectedSuiteOutput =
            '+-------+----------+----------+-------+ '.
            '| Input | Expected | Actual | '.
            '+-------+----------+----------+-------+ '.
            '| 1 | 5 | 5 | ✓ | '.
            '| 2 | 4 | 2.5 | ✗ | '.
            '| 0 | 8 | Division by zero | '.
            '+-------+----------+----------+-------+ ';

        $this->sfoutput = new DummyOutput();
        $this->output = new SymfonyTableOutput($this->sfoutput, 2);
    }

    protected function fetch()
    {
        return $this->sfoutput->fetch();
    }
}
