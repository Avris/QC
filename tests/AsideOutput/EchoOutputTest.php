<?php
namespace Avris\QC\AsideOutput;

class EchoOutputTest extends OutputTest
{
    public function init()
    {
        self::$expectedDebugOutput =
            '3 [3] '.
            'Debug output lines limit reached... '.
            '5 ';

        self::$expectedSuiteOutput =
            '1 5 5 ✓ '.
            '2 4 2.5 ✗ '.
            '0 8 Division by zero ';

        $this->output = new EchoOutput(2);
        ob_start();
    }

    protected function fetch()
    {
        return ob_get_clean();
    }
}
