<?php
namespace Avris\QC\AsideOutput;


class AccumulativeOutputTest extends OutputTest
{

    public function init()
    {
        self::$expectedDebugOutput = [
            ['5' . "\n"],
            [
                [
                    'token' => '3',
                    'stack' => '[3]',
                    'variables' => ''
                ]
            ],
            [],
            true,
        ];

        self::$expectedSuiteOutput = [
            [],
            [],
            [
                [
                    'input' => '1',
                    'expectedOutput' =>'5',
                    'actualOutput' => '5',
                    'ok' => true,
                    'exception' => null,
                ],
                [
                    'input' => '2',
                    'expectedOutput' => '4',
                    'actualOutput' => '2.5',
                    'ok' => false,
                    'exception' => null,
                ],
                [
                    'input' => '0',
                    'expectedOutput' => '8',
                    'actualOutput' => '·',
                    'ok' => false,
                    'exception' => 'Division by zero',
                ]
            ],
            false,
        ];

        $this->output = new AccumulativeOutput(2);
    }

    protected function fetch()
    {
        return [
            $this->output->getOutput(),
            $this->output->getSteps(),
            $this->output->getTests(),
            $this->output->isLimitReached()
        ];
    }
}
