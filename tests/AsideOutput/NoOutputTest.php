<?php
namespace Avris\QC\AsideOutput;

class NoOutputTest extends OutputTest
{
    public function init()
    {
        self::$expectedDebugOutput = '';

        self::$expectedSuiteOutput = '';

        $this->output = new NoOutput();
    }

    protected function fetch()
    {
        return '';
    }
}
