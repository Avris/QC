<?php
namespace Avris\QC\AsideOutput;

use AspectMock\Test as test;
use Avris\QC\Helper;
use Avris\QC\QC;
use Avris\QC\Token\Variable;

abstract class OutputTest extends \PHPUnit_Framework_TestCase
{
    protected static $expectedDebugOutput = '';
    protected static $expectedSuiteOutput = '';

    /** @var AsideOutputInterface */
    protected $output;

    public function testDebug()
    {
        test::double('Avris\QC\Tokenery', ['getVariables' => [new Variable('I', null)]]);
        $qc = new QC();

        $this->init();
        $this->assertInstanceOf('\Avris\QC\AsideOutput\AsideOutputInterface', $this->output);
        $qc->run('3 2+!', null, $this->output);
        $this->assertEquals(static::$expectedDebugOutput, Helper::clearWhitespaces(static::fetch()));

        $this->init();
        $qc->runTestsuite('5I/' . Helper::generateTestsuite([1 => 5, 2 => 4, 0 => 8]), null, $this->output);
        $this->assertEquals(static::$expectedSuiteOutput, Helper::clearWhitespaces(static::fetch()));

        test::clean();
    }

    protected abstract function init();
    protected abstract function fetch();
}
