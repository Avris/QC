## QC • Quite Concise Programming Language ##

**This piece of software is in EARLY development stage**

The idea is to create a **simple** (in terms of complexity, not ease of use) programming language,
that will have quite a **concise syntax**, that will play with the **power of Unicode** and most importantly --
will be **fun to create** ;)

### Documentation ###

Full documentation is available at **[docs.avris.it/qc](http://docs.avris.it/qc/)**

### Sample code ###

This program returns the length of the [Collatz sequence](https://en.wikipedia.org/wiki/Collatz_conjecture)
for a given number:

    # Length of Collatz sequence
    (⪑☯1:{Aa⇓↓a1>:aa2%a3*1+a2/▲=}A↹)I☯

    @0 => 1
    @1 => 1
    @2 => 2
    @3 => 8
    @4 => 3
    @5 => 6
    @[0 1 2 3 4 5] => [1 1 2 8 3 6]

First line is, obviously, just a comment, second one is the actual code, and the rest are test cases --
when you run your code in test mode, it will run for input `0` and check if the output is `1`,
then run for `1` and expect `1` as output, and so on. Notice, in the last case, how simply can a function be applied
to every element of a whole array.

`(⪑☯1:...)` defines a function `☯` that takes `1` argument (= its arity is one) and allows our array trick (`⪑` switch).

`{...:...}` means: while the first part is true, keep executing part two.

`A` is a predefined variable containing an empty array. We put it on the stack.

`a`, `b`, `c`, `d`... are the parameters we passed to the function
(here we only one one, `a`, because our arity is one).

We put the value of `a` (`a⇓`) on the stack.

Function `↓` takes two elements from the stack (i.e. `A` and `a⇓`) and puts `a⇓` at the end of the array `A`.

We put on the stack the variable `a` and the number `1`. Function `>` takes them off and compares. If `a > 1`, it puts
`1 (true)` back on the stack, and `0 (false)` otherwise. So, that would be our while condition.

Then we have `aa2%a3*1+a2/▲=` which is basically `a = a % 2 ? 3*a+1 : a/2`, but written with
[Reverse Polish notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation)
and with `▲` function acting as a teranry operator.

After the *while* loop is finished, we put the length of array `A` (`A↹`) on the stack.
This is return value of our function.
Of course, we could return the whole array (`A`) or its concatenated content (`AS⥋`).

`I☯` means we put the input `I` on the stack, and apply our function to it.

### Installation ###

Install it straight from [Git repository](https://gitlab.com/Avris/QC) or using [Composer](https://getcomposer.org/):

    composer require avris/qc

### Usage ###

#### Online ####

Online interpreter is available at **[qc.avris.it/try](http://qc.avris.it/try)**

#### In console ###

    vendor/bin/qc '2 2+'                        # 4
    vendor/bin/qc '2 2+' -d                     # 4, debug data displayed
    vendor/bin/qc '2 I+' 5                      # 7
    vendor/bin/qc '"foo""bar"+'                 # "foobar"
    vendor/bin/qc -f demo/collatz.qc 5          # 6, source code was read from file
    vendor/bin/qc -f demo/collatz.qc -s         # run related test suite
    vendor/bin/qc -f demo/collatz.qc 5 -dl 300  # debug mode with increased lines limit

#### In PHP ####

    try {
        $qc = new QC();
        $result = $qc->run($code, $input, new EchoOutput());
        echo 'Result: ' . $result;
    } catch (QCException $e) {
        echo 'Error: ' . $e->getMessage();
    }

### Copyright

* **Author:** Andre Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)

