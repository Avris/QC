<?php
namespace Avris\QC;

use Avris\QC\Exception\ErrorHandler;
use Avris\QC\Exception\RuntimeException;

class Helper
{
    /**
     * @param $value
     * @param int $trim
     * @return string
     */
    public static function dumpValue($value, $trim = 10)
    {
        if (is_null($value)) { return '·'; }

        if (is_string($value)) {
            if ($trim) {
                $value = strlen($value) > $trim ? substr($value, 0, $trim-1) . '…' : $value;
            }
            return '"'.$value.'"';
        }

        if (is_float($value) && $value == floor($value)) { return $value . '.'; }

        if (is_array($value)) {
            $return = [];
            foreach ($value as $element) {
                $return[] = self::dumpValue($element, $trim);
            }
            $value = implode(' ', $return);
            if ($trim) {
                $value = strlen($value) > $trim ? substr($value, 0, $trim-1) . '…' : $value;
            }
            return '['.$value.']';
        }

        return (string)$value;
    }

    /**
     * @param string $value
     * @return int|float|string
     */
    public static function parseInput($value)
    {
        $value = (string)$value;

        if ($value == '') { return ''; }

        if ($value == '·') { return null; }

        if (preg_match('/^"(.*)"$/', $value, $matches)) {
            return str_replace('\\"', '"', $matches[1]);
        }

        if (preg_match(Parser::REGEX_ARRAY, $value, $matches)) {
            return self::buildList($value)[1][0];
        }

        return is_numeric($value)
            ? (strpos($value, '.') === false ? (int)$value : (float)$value)
            : $value;
    }

    /**
     * @param string $code
     * @return string
     */
    public static function codeSize($code)
    {
        $handler = new ErrorHandler();
        $code = self::clearCode($code);
        $size = sprintf('%s chars, %s bytes', mb_strlen($code), strlen($code));
        $handler->destruct();
        return $size;
    }

    public static function clearCode($code)
    {
        $code = preg_replace('/#.*\n?/u', '', $code);
        $code = preg_replace('/@.* =>.*\n?/u', '', $code);
        return trim($code);
    }

    public static function generateInput($value)
    {
        if (is_null($value)) { return '·'; }
        if (is_string($value)) { return '"'.$value.'"'; }
        return (string)$value;
    }

    public static function generateTestsuite($tests)
    {
        $out = [];

        foreach ($tests as $input => $expectedOutput) {
            $out[] = '@' . self::generateInput($input) . ' => ' . self::generateInput($expectedOutput);
        }

        return "\n" . implode("\n", $out) . "\n";
    }

    protected static function buildList($nestedList)
    {
        $offset = 0;
        $list = [];

        while ($offset < strlen($nestedList)) {
            $text = substr($nestedList, $offset);
            if (preg_match('/^\[/u', $text)) {
                list($length, $sublist) = self::buildList(substr($text, 1));
                $offset += $length + 1;
                $list[] = $sublist;
            } else if (preg_match('/^\]/u', $text)) {
                $offset++;
                break;
            } else if (preg_match('/^"(((\\\")|[^"])*)"/u', $text, $matches)) {
                $list[] = self::parseInput($matches[0]);
                $offset += strlen($matches[0]);
            } else if (preg_match('/^[^\s\[\]]+/u', $text, $matches)) {
                $list[] = self::parseInput($matches[0]);
                $offset += strlen($matches[0]);
            } else if (preg_match('/^\s+/u', $text, $matches)) {
                $offset += strlen($matches[0]);
            }
        }

        return [$offset, $list];
    }

    /**
     * @param string $code
     * @return array [input => expectedOutput]
     */
    public static function buildSuiteFromCode($code)
    {
        $testsuite = [];
        preg_match_all('/@(.*) => ?(.*)/u', preg_replace('/#.*\n?/u', "\n", $code), $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $testsuite[$match[1]] = self::parseInput(trim($match[2]));
        }
        return $testsuite;
    }

    public static function clearWhitespaces($string)
    {
        if (is_array($string)) { return $string; }
        return preg_replace('/\s+/', ' ', $string);
    }

    public static function isHash($var)
    {
        return is_array($var) && array_keys($var) !== range(0,count($var)-1);
    }

    public static function loadSourceFromFile($path)
    {
        if (!file_exists($path)) {
            throw new RuntimeException(sprintf('Source file %s does not exist', $path));
        }

        $code = file_get_contents($path);
        $dir = dirname($path);

        return preg_replace_callback('/\$(.*)/i', function($m) use ($dir) {
            $path = sprintf('%s/%s.qc', $dir, $m[1]);
            if (!file_exists($path)) {
                throw new RuntimeException(sprintf('Source file %s does not exist', $path));
            }

            return self::clearCode(file_get_contents($path)) . "\n";
        }, $code);
    }
}
