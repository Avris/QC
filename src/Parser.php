<?php
namespace Avris\QC;

use Avris\QC\Exception\ParseException;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\Control\ForControl;
use Avris\QC\Token\Control\FunctionControl;
use Avris\QC\Token\Control\IfElseControl;
use Avris\QC\Token\Control\WhileControl;
use Avris\QC\Token\EmptyToken;

class Parser
{
    const TOKEN_FUNCTION   = 'T_FUNCTION';
    const TOKEN_WHITESPACE = 'T_WHITESPACE';
    const TOKEN_VARIABLE   = 'T_VARIABLE';
    const TOKEN_NULL       = 'T_NULL';
    const TOKEN_NUMBER     = 'T_NUMBER';
    const TOKEN_STRING     = 'T_STRING';
    const TOKEN_ARRAY      = 'T_ARRAY';
    const TOKEN_REGEX      = 'T_REGEX';
    const TOKEN_CONTROL    = 'T_CONTROL';
    const TOKEN_CONSTANT   = 'T_CONSTANT';

    const RESERVED_CHARS   = '\s·A-Za-z0-9\-\.\"\\(\)\[\]\{\}\<\>\:\;¿\?⪑↪↩';

    const REGEX_ARRAY      = '/\[(?:[^\[\]]+|(?R))*\]/u';
    const REGEX_REGEX      = '((?:(?:\\\>)|(?:\\\:)|(?:\\\;)|[^>:;])*)';
    const REGEX_IFELSE     = '/¿([^\:¿\?]*(?R)?[^\:¿\?]*)(?:\:?)([^\:\?]*(?R)?[^\:\?]*)\?/u';
    const REGEX_WHILE      = '/\{([^:\{\}]*(?R)?[^:\{\}]*)(?:\:?)([^:\{\}]*(?R)?[^:\{\}]*)\}/u';

    protected static $terminals = [
        self::TOKEN_WHITESPACE  => '(\s+)',
        self::TOKEN_NULL        => '(·)',
        self::TOKEN_NUMBER      => '(-?[0-9\.]+)',
        self::TOKEN_VARIABLE    => '([A-Za-z])',
        self::TOKEN_STRING      => '"((?:(?:\\\")|[^"])*)"',
        self::TOKEN_REGEX       => null,
        self::TOKEN_FUNCTION    => null,
        self::TOKEN_CONSTANT    => null,
    ];

    /** @var Tokenery */
    protected $tokenery;

    protected $userFunctions = [];

    /**
     * @param Tokenery $tokenery
     */
    public function __construct(Tokenery $tokenery)
    {
        $this->tokenery = $tokenery;

        self::$terminals[self::TOKEN_REGEX] = sprintf(
            '<(?:%s(?:\:([imsxADSUXJu]*))?'.'(\;?)%s)>',
            self::REGEX_REGEX,
            self::REGEX_REGEX
        );

        self::$terminals[self::TOKEN_CONSTANT] =
            '([' . implode('', array_keys($tokenery->getConstants())) . '])';

        $this->updateFunctions();
    }

    /**
     * @param string $code
     * @return AbstractToken[]
     */
    public function parse($code)
    {
        $tokens = [];

        $offset = 0;
        while($offset < strlen($code)) {
            list($token, $increment) = $this->match(substr($code, $offset));
            if (!$token instanceof EmptyToken) {
                $tokens[] = $token;
            }
            $offset += $increment;
        }

        return $tokens;
    }

    protected function match($string)
    {
        if(preg_match(self::REGEX_IFELSE, $string, $matches, PREG_OFFSET_CAPTURE) && $matches[0][1] == 0) {
            return [
                new IfElseControl($this->parse($matches[1][0]), $this->parse($matches[2][0]), $this->tokenery),
                strlen($matches[0][0])
            ];
        }

        if(preg_match(self::REGEX_WHILE, $string, $matches, PREG_OFFSET_CAPTURE) && $matches[0][1] == 0) {
            return [
                new WhileControl($this->parse($matches[1][0]), $this->parse($matches[2][0]), $this->tokenery),
                strlen($matches[0][0])
            ];
        }

        if(preg_match('/↪([^↪↩]*(?R)?[^↪↩]*)↩/u', $string, $matches, PREG_OFFSET_CAPTURE) && $matches[0][1] == 0) {
            return [
                new ForControl($this->parse($matches[1][0]), $this->tokenery),
                strlen($matches[0][0])
            ];
        }

        if(preg_match('/^\((⪑?)([^'.Parser::RESERVED_CHARS.'])\:?(\d+):([^\)]*)\)/u', $string, $matches)) {
            $this->userFunctions[] = $matches[2];
            $this->updateFunctions();
            return [
                new FunctionControl(
                    $matches[2],
                    $matches[1] == '⪑',
                    (int)$matches[3],
                    $this->parse($matches[4]),
                    $this->tokenery
                ),
                strlen($matches[0])
            ];
        }

        foreach(static::$terminals as $name => $pattern) {
            if(preg_match('/^'.$pattern.'/u', $string, $matches)) {
                return [$this->tokenery->buildToken($name, $matches[1], $matches), strlen($matches[0])];
            }
        }

        if(preg_match(self::REGEX_ARRAY, $string, $matches, PREG_OFFSET_CAPTURE) && $matches[0][1] == 0) {
            return [$this->tokenery->buildToken(self::TOKEN_ARRAY, $matches[0][0]), strlen($matches[0][0])];
        }

        throw new ParseException(sprintf('Unable to parse code: %s', $string));
    }

    protected function updateFunctions()
    {
        $functions = array_merge(array_keys($this->tokenery->getFunctions()), $this->userFunctions);
        $functionsNames = str_replace('/', '\/', preg_quote(implode('', $functions)));
        self::$terminals[self::TOKEN_FUNCTION] = '([' . $functionsNames . '])';
    }
}
