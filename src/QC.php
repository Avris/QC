<?php
namespace Avris\QC;

use Avris\QC\AsideOutput\AsideOutputInterface;
use Avris\QC\AsideOutput\NoOutput;
use Avris\QC\Exception\ErrorHandler;
use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Exception\QCException;
use Avris\QC\Exception\RuntimeException;

class QC
{
    const VERSION = '1.0.0';

    /** @var  */
    protected $tokenery;

    public function __construct(Tokenery $tokenery = null)
    {
        $this->tokenery = $tokenery ?: new Tokenery();
    }

    /**
     * @param string $code
     * @param mixed $input
     * @param AsideOutputInterface|null $asideOutput
     * @return mixed
     * @throws QCException
     */
    public function run($code, $input, AsideOutputInterface $asideOutput = null)
    {
        $handler = new ErrorHandler();

        if (!$asideOutput) { $asideOutput = new NoOutput(); }

        $code = Helper::clearCode($code);

        $asideOutput->init();

        $stack = new Stack();
        $tokenery = clone $this->tokenery;
        $tokenery->setDefaultVariables($code, $input);
        $parser = new Parser($tokenery);

        $debugStepCallback = function($token, $stack) use ($asideOutput, $tokenery) {
            $asideOutput->step($token, $stack, $tokenery);
        };

        $outputCallback = function($text) use ($asideOutput) {
            $asideOutput->output($text);
        };

        try {
            foreach ($parser->parse($code) as $token) {
                $token->execute($stack, $debugStepCallback, $outputCallback);
            }
        } catch (QCException $e) {
            $asideOutput->flush();
            throw $e;
        }

        $asideOutput->flush();

        $output = $stack->pop()->getValue();

        $handler->destruct();

        return $output;
    }

    /**
     * @param string $code
     * @param array $testsuite [input => expectedOutput]
     * @param AsideOutputInterface|null $asideOutput
     * @throws RuntimeException
     * @return array
     */
    public function runTestsuite($code, $testsuite = null, AsideOutputInterface $asideOutput = null)
    {
        if (!$asideOutput) { $asideOutput = new NoOutput(); }

        $asideOutput->init();

        if (!$testsuite) {
            $testsuite = Helper::buildSuiteFromCode($code);
        }

        if (!is_array($testsuite)) {
            throw new RuntimeException('Specified testsuite should be an array');
        }

        $stats = ['success' => 0, 'fail' => 0, 'error' => 0];

        foreach ($testsuite as $testInput => $expectedResult) {
            $testInput = Helper::parseInput($testInput);
            try {
                $actualResult = $this->run($code, $testInput);
                $ok = $expectedResult == $actualResult ||
                    (is_float($expectedResult) && is_float($actualResult)
                        && abs($expectedResult - $actualResult) < 0.0000000001);
                $asideOutput->test($testInput, $expectedResult, $actualResult, $ok);
                $stats[$ok ? 'success' : 'fail']++;
            } catch (QCException $e) {
                $asideOutput->test($testInput, $expectedResult, null, false, $e);
                $stats['error']++;
            }
        }

        $stats['ok'] = $stats['fail'] + $stats['error'] == 0;
        $stats['all'] = count($testsuite);

        $asideOutput->flush();

        return $stats;
    }

    /**
     * @param string $name One, non-reserved character
     * @param string $class FQCN that extends \Avris\QC\Token\Func\AbstractFunction
     * @throws InvalidArgumentException
     */
    public function registerFunction($name, $class)
    {
        $handler = new ErrorHandler();
        $this->tokenery->registerFunction($name, $class);
        $handler->destruct();
    }
}
