<?php
namespace Avris\QC\Console;

use Avris\QC\AsideOutput\SymfonyTableOutput;
use Avris\QC\Exception\QCException;
use Avris\QC\Helper;
use Avris\QC\QC;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\DescriptorHelper;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class QCCommand extends Command
{
    /** @var OutputInterface */
    protected $output;

    /** @var FormatterHelper */
    protected $formatter;

    protected function configure()
    {
        $this
            ->addArgument('code', null, 'QC source code to execute')
            ->addArgument('input', null, 'Input data to operate on')
            ->addOption('debug', 'd', null, 'Show debug data')
            ->addOption('debugLimit', 'l', InputOption::VALUE_OPTIONAL, 'Limit number of debug output lines', 100)
            ->addOption('file', 'f', null, 'Treat "code" as a filename')
            ->addOption('suite', 's', null, 'Run for testsuites instead of input')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("\n ".$this->getApplication()->getLongVersion()."\n");

        try {
            list($code, $qcInput, $asideOutput, $suite) = $this->buildParams($input, $output);

            if (!$code) {
                return (new DescriptorHelper())->describe($output, $this);
            }

            $qc = new QC();

            $this->output->writeln('Code size: ' . Helper::codeSize($code) . "\n");

            if (!$suite) {
                $result = $qc->run($code, $qcInput, $asideOutput);
                $this->writeBlock('Result', Helper::dumpValue($result, false));
            } else {
                $result = $qc->runTestsuite($code, $qcInput, $asideOutput);
                $this->writeBlock(
                    $result['ok'] ? 'Successful!' : 'Failed!',
                    $result['success'] . '/'. $result['all'],
                    !$result['ok']
                );
            }
        } catch (QCException $e) {
            $this->writeBlock('Error', $e->getMessage(), true);
        }
    }

    protected function buildParams(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->formatter = $this->getHelper('formatter');

        $code = $input->getArgument('code');
        if ($input->getOption('file')) {
            $code = Helper::loadSourceFromFile(getcwd() . '/' . $code);
        }
        return [
            $code,
            Helper::parseInput($input->getArgument('input')),
            new SymfonyTableOutput($output, $input->getOption('debug') ? $input->getOption('debugLimit') : -1),
            $input->getOption('suite')
        ];
    }

    protected function writeBlock($title, $content, $error = false)
    {
        $this->output->writeln($this->formatter->formatBlock(
            [strtoupper($title), $content],
            $error ? 'error' : 'question',
            true
        ));
    }
}
