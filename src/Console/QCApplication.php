<?php
namespace Avris\QC\Console;

use Avris\QC\QC;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;

class QCApplication extends Application
{
    public function __construct($name = 'QC', $version = QC::VERSION)
    {
        parent::__construct($name, $version);
    }

    public function getCommandName(InputInterface $input)
    {
        return 'qc';
    }

    protected function getDefaultCommands()
    {
        $defaultCommands = parent::getDefaultCommands();

        $defaultCommands[] = new QCCommand('qc');

        return $defaultCommands;
    }

    public function getDefinition()
    {
        $inputDefinition = parent::getDefinition();
        $inputDefinition->setArguments();

        return $inputDefinition;
    }
}