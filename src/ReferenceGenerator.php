<?php
namespace Avris\QC;

use Avris\QC\Token\Func\AbstractFunction;
use Avris\QC\Token\Variable;

class ReferenceGenerator
{
    public function generateReference()
    {
        return [
            'literals' => [
                ['name' => '·', 'insert' => '·', 'description' => 'null'],
                ['name' => '-12', 'description' => 'integer'],
                ['name' => '8. / 8.15 / .8', 'description' => 'float'],
                ['name' => '"13" / "a"', 'description' => 'string'],
                ['name' => '[1 2 abc ["foo bar"]]', 'description' => 'array'],
                ['name' => '<$pattern:$modifiers>', 'insert' => '<:>', 'description' => 'Regular expression (matching)'],
                ['name' => '<$pattern:$modifiers;$replacement>', 'insert' => '<:;>', 'description' => 'Regular expression (replacing)'],
            ],
            'controls' => [
                ['name' => '{$condition:$block}', 'insert' => '{:}', 'description' => 'while ($condition) { $block }'],
                ['name' => '$condition¿$ifBlock:$elseBlock?', 'insert' => '¿:?', 'description' => 'if ($condition) { $ifBlock } else { $elseBlock }'],
                ['name' => '$iterator↪$block↩', 'insert' => '↪↩', 'description' => 'for (1..$iterator) { $block }' . "\n" . 'foreach ($iterator) { $block } if $iterator is an array'],
                ['name' => '($name$arity:$block)', 'insert' => '(:)', 'description' => 'function $name ($arity) { $block }' . "\n" . 'Arity means number of parameters it takes from the stack'],
                ['name' => '(⪑$name$arity:$block)', 'insert' => '(⪑:)', 'description' => 'Providing ⪑-function with an array as a parameter will apply the function to all elements of that array' . "\n" . 'Only works for arity 1 and 2'],
                ['name' => '#comment', 'insert' => '#', 'description' => 'Comment'],
                ['name' => '@$input => $expectedOutput', 'insert' => "\n" . '@ =>', 'description' => 'Test case'],
                ['name' => '$$filename', 'insert' => '$', 'description' => 'Include file $filename'],
            ],
            'defaultVariables' => $this->getDefaultVariables(),
            'constants' => $this->getConstants(),
            'functions' => $this->getFunctions(),
        ];
    }

    protected function getFunctions()
    {
        $tokenery = new Tokenery();
        $functions = [];
        foreach ($tokenery->getFunctions() as $name => $class) {
            $functionClass = 'Avris\QC\Token\Func\\' . $class;
            /** @var AbstractFunction $function */
            $function = new $functionClass($name, $tokenery);
            $functions[] = [
                'name' => $name,
                'insert' => $name,
                'class' => $class,
                'arity' => $function->getArity(),
                'description' => $function->getDescription(),
            ];
        }

        return $functions;
    }

    protected function getDefaultVariables()
    {
        $vars = [];
        $factory = new Tokenery();
        $factory->setDefaultVariables('$code', '$input');
        foreach ($factory->getVariables() as $name => $value) {
            $vars[] = [
                'name' => $name,
                'insert' => $name,
                'description' => $value,
            ];
        }
        return $vars;
    }

    protected function getConstants()
    {
        $constants = [];
        foreach ((new Tokenery())->getConstants() as $constant => $value) {
            $constants[] = ['name' => $constant, 'insert' => $constant];
        }
        return $constants;
    }

    public function generateMarkdown($reference = null)
    {
        if (!$reference) {
            $reference = $this->generateReference();
        }

        $output = '';

        foreach ($reference as $type => $data) {
            $sectionName = $this->camelToWords($type);
            $output .= '### ' . $sectionName . ' ###' . "\n\n";

            $header = $this->generateHeader($data, $sectionName);
            $output .= '| ' . implode(' | ', $header) . ' |' . "\n";
            $output .= str_repeat('| --- ', count($header)) . '|' . "\n";

            foreach ($data as $element) {
                $line = $this->generateLine($sectionName, $element, 'markdown');
                $output .= '| ' . implode(' | ', $line) . ' |' . "\n";
            }

            $output .= "\n";
        }

        return $output;
    }

    public function generateHtml($reference = null, $tableClass = '', $buttonClass = '')
    {
        if (!$reference) {
            $reference = $this->generateReference();
        }

        $output = '';

        foreach ($reference as $type => $data) {
            $sectionName = $this->camelToWords($type);

            $output .= '<table class="'.$tableClass.'" data-section="'.$sectionName.'">';

            $header = $this->generateHeader($data, $sectionName);
            $output .= '<tr><th>'.implode('</th><th>', $header).'</th></tr>';

            foreach ($data as $element) {
                $line = $this->generateLine($sectionName, $element, 'html');
                $output .= '<tr><td>'.implode('</td><td>', $line).'</td></tr>';
            }

            $output .= '</table>';
        }

        return str_replace('{btn_class}', $buttonClass, $output);
    }

    protected function camelToWords($string)
    {
        return ucfirst(strtolower(trim(
            preg_replace('/(?<=\\w)(?=[A-Z])/',' $1', trim($string))
        )));
    }

    /**
     * @param $data
     * @param $sectionName
     * @return array
     */
    protected function generateHeader($data, $sectionName)
    {
        $header = [];
        foreach (array_keys($data[0]) as $key) {
            if ($key == 'name') {
                $header[] = substr($sectionName, 0, -1);
            } else if ($key == 'insert') {
                continue;
            } else {
                $header[] = $this->camelToWords($key);
            }
        }
        return $header;
    }

    /**
     * @param string $section
     * @param array $element
     * @param string $mode
     * @return array
     */
    protected function generateLine($section, $element, $mode)
    {
        $line = [];
        $insert = isset($element['insert']) ? $element['insert'] : null;
        foreach ($element as $key => $value) {
            if ($key == 'insert') { continue; }
            if ($key == 'name') {
                if ($mode == 'markdown') {
                    $line[] = '`' . $this->dumpVariable($value) . '`';
                } elseif ($mode == 'html') {
                    $line[] = $insert
                        ? '<button type="button" class="{btn_class}" data-insert="'.$insert.'"><code>'
                        . $this->htmlEscape($this->dumpVariable($value))
                        . '</code></button>'
                        : '<code>' . $this->htmlEscape($this->dumpVariable($value)) . '</code>';
                }
            } else {
                $cell = $this->dumpVariable($value, $section == 'Default variables');
                $line[] = $mode == 'html' ? $this->htmlEscape($cell) : $cell;
            }
        }
        return $line;
    }

    protected function dumpVariable($value, $varDump = false)
    {
        if ($value instanceof Variable) {
            $value = $value->getValue();
        }

        if (!$varDump) {
            return str_replace("\n", '<br/>', $value);
        }

        if (is_array($value)) {
            return json_encode($value);
        }

        if (is_string($value)) {
            return substr($value, 0, 1) == '$' ? $value : '"' . $value .'"';
        }

        return $value;
    }

    protected function htmlEscape($string)
    {
        return str_replace('&lt;br/&gt;', '<br/>', htmlspecialchars($string));
    }
}
