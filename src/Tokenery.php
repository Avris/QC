<?php
namespace Avris\QC;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Exception\ParseException;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\EmptyToken;
use Avris\QC\Token\Func\AbstractFunction;
use Avris\QC\Token\Func\FunctionPromise;
use Avris\QC\Token\Func\QCFunction;
use Avris\QC\Token\Literal;
use Avris\QC\Token\Regex;
use Avris\QC\Token\Variable;

class Tokenery
{
    private $functions = [
        '=' => 'Assign',
        '¡' => 'PrintFunc',
        '!' => 'PrintNewLine',
        '↟' => 'Pop',
        '▲' => 'Ternary',
        '⇓' => 'Value',
        '☉' => 'ArrayToVars',
        'ƶ' => 'Sleep',

        '+' => 'Math\Plus',
        '-' => 'Math\Minus',
        '*' => 'Math\Times',
        '/' => 'Math\Divide',
        '%' => 'Math\Modulo',
        '^' => 'Math\Power',
        '√' => 'Math\Sqrt',
        '㏒' => 'Math\Log',

        '‡' => 'Math\Increment',
        '⸗' => 'Math\Decrement',
        '⩲' => 'Math\Add',
        '±' => 'Math\Negate',
        '‖' => 'Math\Abs',
        '⌋' => 'Math\Floor',
        '⌉' => 'Math\Ceil',
        '≊' => 'Math\Round',
        '‼' => 'Math\Factorial',

        '⋇' => 'Math\IsNumeric',
        '℥' => 'Math\Random',
        '⇅' => 'Math\Base',
        '⩥' => 'Math\LimitRange',

        '⊿' => 'Math\Hypotenuse',
        '∡' => 'Math\DegToRad',
        '⦛' => 'Math\RadToDeg',
        '⊾' => 'Math\Trig',

        '≟' => 'Compare\Equals',
        '≠' => 'Compare\NotEquals',
        '>' => 'Compare\Greater',
        '≥' => 'Compare\GreaterEqual',
        '<' => 'Compare\Less',
        '≤' => 'Compare\LessEqual',

        '∨' => 'Logic\LogicalOr',
        '∧' => 'Logic\LogicalAnd',
        '⊻' => 'Logic\LogicalXor',
        '~' => 'Logic\LogicalNot',

        '↹' => 'StringArray\Length',
        '↓' => 'StringArray\PushTop',
        '↑' => 'StringArray\PushBottom',
        '⊕' => 'StringArray\Sum',
        '⊗' => 'StringArray\Product',
        '⥍' => 'StringArray\Explode',
        '⥋' => 'StringArray\Implode',
        '⥏' => 'StringArray\StringToArray',
        '⥎' => 'StringArray\ArrayToString',
        '⥬' => 'StringArray\CharToAscii',
        '⥪' => 'StringArray\AsciiToChar',
        'ₓ' => 'StringArray\Range',
        '₁' => 'StringArray\RangeOne',
        '₀' => 'StringArray\RangeZero',
        'ᴙ' => 'StringArray\Reverse',
        '☌' => 'StringArray\FindIndex',
        '☍' => 'StringArray\FindSubrange',
        '⌊' => 'StringArray\Min',
        '⌈' => 'StringArray\Max',
        '∈' => 'StringArray\InArray',
        'Φ' => 'StringArray\FindPosition',

        '≈' => 'StringArray\AlmostEqual',
        '≉' => 'StringArray\NotAlmostEqual',
        '⥵' => 'StringArray\Transform',
    ];

    protected $constants = [
        '∞' => INF,
        'π' => M_PI,
        'ℯ' => M_EULER,
    ];

    /** @var Variable[] */
    protected $variables = [];

    /** @var string[] */
    protected $usedVariables = [];

    /**
     * @param string $token
     * @param string $value
     * @param array $matches
     * @throws
     * @return AbstractToken
     */
    public function buildToken($token, $value, $matches = [])
    {
        switch ($token) {
            case Parser::TOKEN_WHITESPACE:
                return new EmptyToken();
            case Parser::TOKEN_FUNCTION:
                if (!isset($this->functions[$value])) {
                    return new FunctionPromise($value, $this);
                }
                $class = $this->functions[$value];
                if (is_object($class)) {
                    return clone $class;
                }
                $realClass = strpos($class, '\\') === 0 ? $class : 'Avris\QC\Token\Func\\' . $class;
                return new $realClass($value, $this);
            case Parser::TOKEN_NUMBER:
            case Parser::TOKEN_STRING:
            case Parser::TOKEN_NULL:
                return new Literal($token, $value);
            case Parser::TOKEN_ARRAY:
                return new Literal($token, Helper::parseInput($value));
            case Parser::TOKEN_CONSTANT:
                if (!isset($this->constants[$value])) {
                    throw new ParseException(sprintf('Unrecognised constant "%s"', $value));
                }
                return new Literal(Parser::TOKEN_NUMBER, $this->constants[$value]);
            case Parser::TOKEN_VARIABLE:
                return $this->getVariable($value);
            case Parser::TOKEN_REGEX:
                return new Regex(
                    Regex::unquote($matches[1]),
                    $matches[2],
                    $matches[3] == ';',
                    Regex::unquote($matches[4])
                );
            default:
                throw new ParseException(sprintf('Unrecognised token %s (%s)', $token, $value));
        }
    }

    /**
     * @return Variable[]
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @return Variable[]
     */
    public function getUsedVariables()
    {
        $variables = [];
        foreach ($this->variables as $name => $variable) {
            if (in_array($name, $this->usedVariables)) {
                $variables[] = $variable;
            }
        }
        return $variables;
    }

    /**
     * @param string $name
     * @return Variable
     */
    public function getVariable($name)
    {
        $this->updateUsedVariables($name);

        return isset($this->variables[$name])
            ? $this->variables[$name]
            : $this->variables[$name] = new Variable($name);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setVariable($name, $value)
    {
        $this->variables[$name] = isset($this->variables[$name])
            ? $this->variables[$name]->setValue($value)
            : new Variable($name, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setVariableForce($name, $value)
    {
        $this->updateUsedVariables($name);

        $this->variables[$name] = new Variable($name, $value);

        return $this;
    }

    protected function updateUsedVariables($name)
    {
        if (!in_array($name, $this->usedVariables)) {
            $this->usedVariables[] = $name;
        }
    }

    /**
     * @param string $code
     * @param mixed $input
     * @return $this
     */
    public function setDefaultVariables($code, $input)
    {
        return $this
            ->setVariable('I', $input)
            ->setVariable('C', $code)
            ->setVariable('Z', 0)
            ->setVariable('J', 1)
            ->setVariable('T', 10)
            ->setVariable('E', "")
            ->setVariable('S', " ")
            ->setVariable('K', ",")
            ->setVariable('D', ".")
            ->setVariable('A', [])
            ->setVariable('L', 'abcdefghijklmnopqrstuvwxyz')
            ->setVariable('U', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')
            ->setVariable('N', '0123456789')
        ;
    }

    /**
     * @return array
     */
    public function getConstants()
    {
        return $this->constants;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return $this->functions;
    }

    /**
     * @param string $name One, non-reserved character
     * @param string $class FQCN that extends \Avris\QC\Token\Func\AbstractFunction
     * @return bool
     * @throws InvalidArgumentException
     */
    public function registerFunction($name, $class)
    {
        $this->assertFunctionNameCorrect($name);

        $fqcn = '\\' . ltrim($class, '\\');
        if (!class_exists($fqcn)) {
            throw new InvalidArgumentException(sprintf('Class %s does not exist', $fqcn));
        }

        $this->assertInstanceOfAbstractFunction($fqcn);

        $this->functions[$name] = $fqcn;

        return true;
    }

    /**
     * @param string $name
     * @param QCFunction $function
     * @return bool
     * @throws InvalidArgumentException
     */
    public function registerInternalFunction($name, QCFunction $function)
    {
        $this->assertFunctionNameCorrect($name);

        $this->functions[$name] = $function;

        return true;
    }

    protected function assertFunctionNameCorrect($name)
    {
        if (isset($this->functions[$name])) {
            throw new InvalidArgumentException(sprintf('Function "%s" is already registered', $name));
        }

        if (mb_strlen($name) != 1 || preg_match('/['.Parser::RESERVED_CHARS.']/u', $name)) {
            throw new InvalidArgumentException('Function name has to be one, non-reserved character');
        }
    }

    protected function assertInstanceOfAbstractFunction($class)
    {
        $obj = is_object($class) ? $class : new $class('', $this);
        if (!$obj instanceof AbstractFunction) {
            throw new InvalidArgumentException(sprintf(
                'Class %s is not an instance of %s',
                $class,
                '\Avris\QC\Token\Func\AbstractFunction'
            ));
        }
    }

    function __clone()
    {
        $vars = [];
        foreach ($this->variables as $key => $value) {
            $vars[$key] = clone $value;
        }
        $this->variables = $vars;
    }
}
