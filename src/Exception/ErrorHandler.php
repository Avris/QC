<?php
namespace Avris\QC\Exception;

class ErrorHandler
{
    private $errorReporting;
    private $displayErrors;
    private $encoding;

    private $desctructed = false;

    public function __construct()
    {
        $this->errorReporting = error_reporting();
        $this->displayErrors = ini_get('display_errors');
        $this->encoding = mb_internal_encoding();

        error_reporting(E_ALL);
        ini_set('display_errors', 0);
        mb_internal_encoding('UTF-8');
        set_error_handler(function($type, $message, $file, $line) {
            if (0 === error_reporting()) { return false; }
            throw new RuntimeException(sprintf('%s in file %s line %s', $message, $file, $line));
        }, E_ALL);
    }

    private function __clone() {}

    public function destruct()
    {
        if ($this->desctructed) {
            throw new RuntimeException('Cannot destruct ErrorHandler again');
        }
        error_reporting($this->errorReporting);
        ini_set('display_errors', $this->displayErrors);
        mb_internal_encoding($this->encoding);
        restore_error_handler();
        $this->desctructed = true;
    }
}
