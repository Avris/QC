<?php
namespace Avris\QC;

use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\EmptyToken;

class Stack
{
    /** @var AbstractToken[] */
    protected $elements = [];

    /** @var int */
    protected $pointer = -1;

    /**
     * @param AbstractToken $element
     * @return $this
     */
    public function push(AbstractToken $element)
    {
        $this->elements[++$this->pointer] = $element;
        return $this;
    }

    /**
     * @return AbstractToken|null
     */
    public function pop()
    {
        if ($this->pointer < 0) {
            return new EmptyToken();
        }

        $element = $this->elements[$this->pointer];
        unset($this->elements[$this->pointer--]);

        return $element;
    }

    /**
     * @return AbstractToken[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    public function getSize()
    {
        return $this->pointer + 1;
    }

    public function __toString()
    {
        return '[' . implode(', ', $this->getElements()) . ']';
    }
}
