<?php
namespace Avris\QC\AsideOutput;

use Avris\QC\Exception\QCException;
use Avris\QC\Helper;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;

class AccumulativeOutput extends LimitedOutput
{
    protected $output = [];

    protected $steps = [];

    protected $tests = [];

    /** @var int */
    protected $debugLimit;

    /** @var int */
    protected $debugCount;

    public function output($text)
    {
        $this->output[] = $text;
    }

    public function step(AbstractToken $token, Stack $stack, Tokenery $tokenery)
    {
        if (++$this->debugCount >= $this->debugLimit) {
            return;
        }

        $this->steps[] = [
            'token' => $token->getDump(),
            'stack' => (string) $stack,
            'variables' => implode(', ', $tokenery->getUsedVariables()),
        ];
    }

    public function test($testInput, $expectedResult, $actualResult, $ok, QCException $exception = null)
    {
        $this->tests[] = [
            'input' => Helper::dumpValue($testInput, 30),
            'expectedOutput' => Helper::dumpValue($expectedResult, 30),
            'actualOutput' => Helper::dumpValue($actualResult, 30),
            'ok' => $ok,
            'exception' => $exception ? $exception->getMessage() : null,
        ];
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function getSteps()
    {
        return $this->steps;
    }

    public function getTests()
    {
        return $this->tests;
    }
}
