<?php
namespace Avris\QC\AsideOutput;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\OutputInterface;

class SymfonyTableOutput extends AccumulativeOutput
{
    /** @var OutputInterface */
    protected $output;

    public function __construct(OutputInterface $output, $debugLimit = 100)
    {
        $this->output = $output;
        parent::__construct($debugLimit);
    }

    public function output($text)
    {
        $this->output->write($text);
    }

    public function flush()
    {
        if (count($this->steps)) {
            $table = new Table($this->output);
            $table->setHeaders(['Token', 'Stack', 'Variables' ]);
            $table->setRows($this->steps);
            if ($this->debugCount >= $this->debugLimit) {
                $table->addRow(new TableSeparator());
                $table->addRow([new TableCell(
                    '<comment>Debug output lines limit reached...</comment>',
                    ['colspan' => 3]
                )]);
            }
            $table->render();
            $this->output->writeln('');
        }

        if (count($this->tests)) {
            $table = new Table($this->output);
            $table->setHeaders(['Input', 'Expected', new TableCell('Actual', ['colspan' => 2])]);
            foreach ($this->tests as $test) {
                $row = [$test['input'], $test['expectedOutput']];

                if ($test['exception']) {
                    $row[] = new TableCell('<error>'.$test['exception'].'</error>', ['colspan' => 2]);
                } else {
                    $row[] = $test['actualOutput'];
                    $row[] = $test['ok'] ? '<question>  ✓  </question>' : '<error>  ✗  </error>';
                }

                $table->addRow($row);
            }

            $table->render();
            $this->output->writeln('');
        }
    }
}
