<?php
namespace Avris\QC\AsideOutput;

abstract class LimitedOutput implements AsideOutputInterface
{
    /** @var int */
    protected $debugLimit;

    /** @var int */
    protected $debugCount;

    public function __construct($debugLimit = 100)
    {
        $this->debugLimit = $debugLimit;
    }

    public function init()
    {
        $this->debugCount = 0;
    }

    public function flush()
    {
    }

    public function isLimitReached()
    {
        return $this->debugCount >= $this->debugLimit;
    }
}
