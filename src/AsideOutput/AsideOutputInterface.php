<?php
namespace Avris\QC\AsideOutput;

use Avris\QC\Exception\QCException;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;

interface AsideOutputInterface
{
    public function init();

    public function output($text);

    public function step(AbstractToken $token, Stack $stack, Tokenery $tokenery);

    public function test($testInput, $expectedResult, $actualResult, $ok, QCException $exception = null);

    public function flush();
}