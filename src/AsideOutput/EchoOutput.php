<?php
namespace Avris\QC\AsideOutput;

use Avris\QC\Exception\QCException;
use Avris\QC\Helper;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;

class EchoOutput extends LimitedOutput
{
    public function output($text)
    {
        echo $text;
    }

    public function step(AbstractToken $token, Stack $stack, Tokenery $tokenery)
    {
        if (++$this->debugCount >= $this->debugLimit) {
            if ($this->debugCount == $this->debugLimit) {
                echo 'Debug output lines limit reached...' . "\n";
            }
            return;
        }

        echo
            $token->getDump() . "\t" .
            (string) $stack  . "\t" .
            implode(', ', $tokenery->getUsedVariables()) . "\n";
    }

    public function test($testInput, $expectedResult, $actualResult, $ok, QCException $exception = null)
    {
        echo
            Helper::dumpValue($testInput) . "\t" .
            Helper::dumpValue($expectedResult) . "\t" .
            ($exception
                ? $exception->getMessage()
                : (Helper::dumpValue($actualResult) . "\t" .
                    ($ok ? '  ✓  ' : '  ✗  ')
                )
            ) . "\n";
    }

    public function flush()
    {
        echo "\n";
    }
}
