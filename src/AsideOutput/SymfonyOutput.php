<?php
namespace Avris\QC\AsideOutput;

use Avris\QC\Exception\QCException;
use Avris\QC\Helper;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;
use Symfony\Component\Console\Output\OutputInterface;

class SymfonyOutput extends LimitedOutput
{
    /** @var OutputInterface */
    protected $output;

    public function __construct(OutputInterface $output, $debugLimit = 100)
    {
        $this->output = $output;
        parent::__construct($debugLimit);
    }

    public function output($text)
    {
        $this->output->write($text);
    }

    public function step(AbstractToken $token, Stack $stack, Tokenery $tokenery)
    {
        if (++$this->debugCount >= $this->debugLimit) {
            if ($this->debugCount == $this->debugLimit) {
                $this->output->writeln('<comment>Debug output lines limit reached...</comment>');
            }
            return;
        }

        $this->output->writeln(
            $token->getDump() . "\t" .
            (string) $stack  . "\t" .
            implode(', ', $tokenery->getUsedVariables())
        );
    }

    public function test($testInput, $expectedResult, $actualResult, $ok, QCException $exception = null)
    {
        $this->output->writeln(
            Helper::dumpValue($testInput) . "\t" .
            Helper::dumpValue($expectedResult) . "\t" .
            ($exception
                ? '<error>' . $exception->getMessage() . '</error>'
                : (Helper::dumpValue($actualResult) . "\t" .
                    ($ok ? '<question>  ✓  </question>' : '<error>  ✗  </error>')
                )
            )
        );
    }

    public function flush()
    {
        $this->output->writeln('');
    }
}
