<?php
namespace Avris\QC\Token;

use Avris\QC\Stack;

class EmptyToken extends AbstractToken
{
    public function __construct()
    {
        parent::__construct(null, null);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
    }
}
