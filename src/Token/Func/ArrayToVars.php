<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\EmptyToken;

class ArrayToVars extends AbstractFunctionOne
{
    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $array = $stack->pop();
        if ($array instanceof EmptyToken) {
            $array = $this->tokenery->getVariable('I');
        }

        if (!$array instanceof AbstractValue || !$array->isArray()) {
            throw new InvalidArgumentException('The argument of "☉" has to be an array');
        }

        $var = 'a';
        foreach ($array->getValue() as $element) {
            $this->tokenery->setVariable(
                $var++,
                $element instanceof AbstractValue ? $element->getValue() : $element
            );
        }

        $debug($this, $stack);
    }

    public function pushesResult()
    {
        return false;
    }

    public function run($arg)
    {
    }

    public function getDescription()
    {
        return 'Iterates over $a (or I if stack is empty) and sets its values as variables a, b, c, ... consecutively';
    }

}
