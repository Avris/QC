<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionOne;

class AsciiToChar extends AbstractFunctionOne
{
    protected function run($arg)
    {
        $asciis = $arg[0]->isArray()
            ? $arg[0]->getValue()
            : [$arg[0]->getValue()];

        $string = '';
        foreach ($asciis as $ascii) {
            $string .= chr($ascii);
        }

        return $string;
    }

    public function getDescription()
    {
        return 'Returns character coresponding to given ASCII value';
    }
}
