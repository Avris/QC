<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionOne;

class ArrayToString extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "⥎" has to be an array');
        }

        return implode('', $arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Joins array $a using string "" as a glue';
    }
}
