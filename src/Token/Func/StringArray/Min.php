<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Func\AbstractFunctionOne;

class Min extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('The argument of "⌊" has to be an array');
        }

        $min = INF;
        foreach ($arg[0]->getValue() as $element) {
            if ($element < $min) {
                $min = $element instanceof AbstractValue ? $element->getValue() : $element;
            }
        }
        return $min;
    }

    public function getDescription()
    {
        return 'Minimum of array\'s elements';
    }
}
