<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionRegex;
use Avris\QC\Token\Regex;

class AlmostEqual extends AbstractFunctionRegex
{
    protected function run($arg)
    {
        /** @var Regex $regex */
        list($regex, $string) = $this->handleArguments($arg);

        $matching = preg_match($regex->getPattern(), $string, $matches);
        $this->tokenery->setVariable('R', $matches);
        return $matching ? 1 : 0;
    }

    public function getDescription()
    {
        return 'Checks if string $a matches regex $b (commutative)' . "\n" .
            'Sets the R variable to found matches';
    }
}
