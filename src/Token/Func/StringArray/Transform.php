<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionRegex;
use Avris\QC\Token\Regex;

class Transform extends AbstractFunctionRegex
{
    protected function run($arg)
    {
        /** @var Regex $regex */
        list($regex, $string) = $this->handleArguments($arg);

        return preg_replace($regex->getPattern(), $regex->getReplacement(), $string);
    }

    public function getDescription()
    {
        return 'Transforms string $a according to regex $b (commutative)';
    }
}
