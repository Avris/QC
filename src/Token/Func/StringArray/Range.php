<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Range extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return range($arg[1]->getValue(), $arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Generate a range of integers between $a and $b';
    }
}
