<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;
use Avris\QC\Token\Variable;

class PushTop extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "↓" has to be an array');
        }

        $value = $arg[1]->getValue();
        $value[] = $arg[0];
        return $arg[1] instanceof Variable
            ? $arg[1]->setValue($value)
            : $value;
    }

    public function getDescription()
    {
        return 'Put $b at the end of array $a';
    }

}
