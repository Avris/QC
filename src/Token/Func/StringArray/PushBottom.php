<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;
use Avris\QC\Token\Variable;

class PushBottom extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "↑" has to be an array');
        }

        $array = $arg[1]->getValue();
        array_unshift($array, $arg[0]->getValue());
        return $arg[1] instanceof Variable
            ? $arg[1]->setValue($array)
            : $array;
    }

    public function getDescription()
    {
        return 'Put $b at the beginning of array $a';
    }

}
