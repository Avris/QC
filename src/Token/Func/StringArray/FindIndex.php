<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class FindIndex extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "☌" has to be an array');
        }

        $arr = $arg[1]->getValue();
        $index = $arg[0]->getValue();

        return isset($arr[$index]) ? $arr[$index] : null;
    }

    public function getDescription()
    {
        return '$a[$b] or null';
    }
}
