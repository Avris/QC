<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Func\AbstractFunctionOne;

class Product extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('The argument of "⊗" has to be an array');
        }

        $product = 1;
        foreach ($arg[0]->getValue() as $element) {
            $product *= $element instanceof AbstractValue ? $element->getValue() : $element;
        }
        return $product;
    }

    public function getDescription()
    {
        return 'Product of array\'s elements';
    }
}
