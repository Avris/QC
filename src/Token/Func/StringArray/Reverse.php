<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionOne;

class Reverse extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('The argument of "ᴙ" has to be an array');
        }

        return array_reverse($arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Reverses an array';
    }
}
