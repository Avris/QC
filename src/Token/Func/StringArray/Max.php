<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Func\AbstractFunctionOne;

class Max extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('The argument of "⌈" has to be an array');
        }

        $max = -INF;
        foreach ($arg[0]->getValue() as $element) {
            if ($element > $max) {
                $max = $element instanceof AbstractValue ? $element->getValue() : $element;
            }
        }
        return $max;
    }

    public function getDescription()
    {
        return 'Maximum of array\'s elements';
    }
}
