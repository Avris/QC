<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class Implode extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "⥋" has to be an array');
        }

        return implode($arg[0]->getValue(), $arg[1]->getValue());
    }

    public function getDescription()
    {
        return 'Joins array $a using string $b as a glue';
    }
}
