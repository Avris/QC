<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunction;

class FindSubrange extends AbstractFunction
{
    protected function run($arg)
    {
        if (!$arg[2]->isArray()) {
            throw new InvalidArgumentException('Argument 1 of "☍" has to be an array');
        }

        return array_slice($arg[2]->getValue(), $arg[1]->getValue(), $arg[0]->getValue());
    }

    public function getArity()
    {
        return 3;
    }

    public function getDescription()
    {
        return 'Subarray of $a starting with $b (negative $b means counting from the end) of length $c';
    }
}
