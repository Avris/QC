<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class InArray extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('Argument 2 of "∈" has to be an array');
        }

        return in_array($arg[1]->getValue(), $arg[0]->getValue()) ? 1 : 0;
    }

    public function getDescription()
    {
        return 'Checks if $a is an element of array $b';
    }
}
