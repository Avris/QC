<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionOne;

class RangeOne extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return range(1, $arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Generate a range of integers between 1 and $a';
    }
}
