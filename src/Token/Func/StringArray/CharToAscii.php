<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Parser;
use Avris\QC\Token\Func\AbstractFunctionOne;
use Avris\QC\Token\Literal;

class CharToAscii extends AbstractFunctionOne
{
    protected function run($arg)
    {
        $val = $arg[0]->getValue();
        $char = strlen($val) == 1
            ? $val
            : new Literal(Parser::TOKEN_ARRAY, str_split($val));

        return $this->handleScalarArrayOne($char, function($a) { return ord($a); });
    }

    public function getDescription()
    {
        return 'Returns ASCII value of a character $a';
    }
}
