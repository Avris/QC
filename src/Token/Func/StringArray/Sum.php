<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Func\AbstractFunctionOne;

class Sum extends AbstractFunctionOne
{
    protected function run($arg)
    {
        if (!$arg[0]->isArray()) {
            throw new InvalidArgumentException('The argument of "⊕" has to be an array');
        }

        $sum = 0;
        foreach ($arg[0]->getValue() as $element) {
            $sum += $element instanceof AbstractValue ? $element->getValue() : $element;
        }
        return $sum;
    }

    public function getDescription()
    {
        return 'Sum of array\'s elements';
    }
}
