<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class FindPosition extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if ($arg[1]->isArray()) {
            $position = array_search($arg[0]->getValue(), $arg[1]->getValue());
            return $position === false ? -1 : $position;
        }

        $position = strpos((string)$arg[1]->getValue(), (string)$arg[0]->getValue());
        return $position === false ? -1 : $position;
    }

    public function getDescription()
    {
        return 'Finds the position of the first occurance of $a in string/array $b (or -1 if not found)';
    }
}
