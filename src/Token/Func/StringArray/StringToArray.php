<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionOne;

class StringToArray extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return str_split($a); });
    }

    public function getDescription()
    {
        return 'Splits array $a letter by letter';
    }
}
