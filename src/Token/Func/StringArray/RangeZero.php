<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionOne;

class RangeZero extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return range(0, $arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Generate a range of integers between 0 and $a';
    }
}
