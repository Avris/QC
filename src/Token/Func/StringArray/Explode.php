<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Explode extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return explode($arg[0]->getValue(), $arg[1]->getValue());
    }

    public function getDescription()
    {
        return 'Splits array $a with divider $b';
    }
}
