<?php
namespace Avris\QC\Token\Func\StringArray;

class NotAlmostEqual extends AlmostEqual
{
    protected function run($arg)
    {
        return parent::run($arg) ? 0 : 1;
    }

    public function getDescription()
    {
        return 'Checks if string $a does not match regex $b (commutative)' . "\n" .
            'Sets the R variable to found matches';
    }
}
