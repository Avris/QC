<?php
namespace Avris\QC\Token\Func\StringArray;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Length extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $arg[0]->isArray() ? count($arg[0]->getValue()) : strlen($arg[0]->getValue());
    }

    public function getDescription()
    {
        return 'Length of string or size of array';
    }
}
