<?php
namespace Avris\QC\Token\Func;

abstract class AbstractFunctionOne extends AbstractFunction
{
    public function getArity()
    {
        return 1;
    }
}
