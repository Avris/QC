<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Stack;

class PrintNewLine extends AbstractFunctionOne
{
    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $arguments = $this->fetchArguments($stack);
        $result = $this->run($arguments);
        $output($result . "\n");
        $this->pushResult($stack, $result);
        $debug($this, $stack);
    }

    protected function run($arg)
    {
        return $arg[0]->getValue();
    }

    public function getDescription()
    {
        return 'Prints $a and a new line';
    }
}
