<?php
namespace Avris\QC\Token\Func;

class Ternary extends AbstractFunction
{
    protected function run($arg)
    {
        return $arg[2]->getValue()
            ? $arg[1]
            : $arg[0];
    }

    public function getArity()
    {
        return 3;
    }

    public function getDescription()
    {
        return '$a ? $b : $c' . "\n" .'All the expressions get executed. If you don\'t want that, use ¿:?';
    }
}
