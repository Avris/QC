<?php
namespace Avris\QC\Token\Func;

abstract class AbstractFunctionTwo extends AbstractFunction
{
    public function getArity()
    {
        return 2;
    }
}
