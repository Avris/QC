<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Parser;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\EmptyToken;
use Avris\QC\Token\Literal;
use Avris\QC\Token\Variable;
use Avris\QC\Tokenery;

abstract class AbstractFunction extends AbstractToken
{
    /** @var Tokenery */
    protected $tokenery;

    public function __construct($value, Tokenery $tokenery)
    {
        $this->tokenery = $tokenery;
        parent::__construct(Parser::TOKEN_FUNCTION, $value);
    }

    /**
     * @param Stack $stack
     * @param callable $debug
     * @param callable $output
     * @throws InvalidArgumentException
     */
    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $arguments = $this->fetchArguments($stack);
        $result = $this->run($arguments);
        $this->pushResult($stack, $result);
        $debug($this, $stack);
    }

    /**
     * @return int
     */
    public abstract function getArity();

    /**
     * @return bool
     */
    public function pushesResult()
    {
        return true;
    }

    public abstract function getDescription();

    /**
     * @param Stack $stack
     * @return array|AbstractValue[]
     * @throws InvalidArgumentException
     */
    protected function fetchArguments(Stack $stack)
    {
        $arguments = [];
        $count = $requiredCount = $this->getArity();
        while($count--) {
            $argument = $stack->pop();
            if ($argument instanceof EmptyToken) {
                throw new InvalidArgumentException(sprintf(
                    'Function "%s" requires %s parameter%s, %s given',
                    $this->getValue(),
                    $requiredCount,
                    $requiredCount > 1 ? 's' : '',
                    $requiredCount - $count - 1
                ));
            }
            $arguments[] = $argument;
        }

        return $arguments;
    }

    /**
     * @param AbstractValue[] $arg
     */
    protected abstract function run($arg);

    protected function pushResult(Stack $stack, $result)
    {
        if (!$this->pushesResult()) {
            return;
        }

        $token = is_null($result)
            ? Parser::TOKEN_NULL
            : (is_array($result)
                ? Parser::TOKEN_ARRAY
                : (is_string($result) ? Parser::TOKEN_STRING : Parser::TOKEN_NUMBER));

        $stack->push($result instanceof AbstractToken ? $result : (new Literal($token, $result)));
    }

    /**
     * @param AbstractValue|mixed $a
     * @param callable $op
     * @return array|AbstractToken
     */
    protected function handleScalarArrayOne($a, callable $op)
    {
        if ($a instanceof AbstractValue && $a->isArray() || is_array($a)) {
            $modified = [];
            foreach ($a->getValue() as $el) {
                $modified[] = $this->handleScalarArrayOne($el instanceof AbstractValue ? $el->getValue() : $el, $op);
            }
        } else {
            $modified = $op($a instanceof AbstractValue ? $a->getValue() : $a);
        }

        return $a instanceof Variable ? $a->setValue($modified) : $modified;
    }

    protected function handleScalarArrayTwo(AbstractValue $a, AbstractValue $b, callable $op, $default = 0)
    {
        if (!$a->isArray() && !$b->isArray()) {
            return $op($a->getValue(), $b->getValue());
        }

        if ($a->isArray() && !$b->isArray()) {
            return $this->doHandleScalarArray($a->getValue(), [], $op, $b->getValue());
        }

        if (!$a->isArray() && $b->isArray()) {
            return $this->doHandleScalarArray([], $b->getValue(), $op, $a->getValue());
        }

        return $this->doHandleScalarArray($a->getValue(), $b->getValue(), $op, $default);
    }

    private function doHandleScalarArray(array $a, array $b, callable $operation, $default)
    {
        $length = max(count($a), count($b));

        $res = [];
        for ($i = 0; $i < $length; $i++) {
            $aEl = isset($a[$i]) ? $a[$i] : $default;
            $bEl = isset($b[$i]) ? $b[$i] : $default;
            $res[$i] = $operation($aEl, $bEl);
        }
        return $res;
    }

}
