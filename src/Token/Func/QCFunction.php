<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Control\AbstractControl;
use Avris\QC\Tokenery;

class QCFunction extends AbstractControl
{
    /** @var string */
    protected $name;

    /** @var bool */
    protected $handlesArrays;

    /** @var int */
    protected $arity;

    /** @var AbstractToken[] */
    protected $block;

    /** @var callable */
    protected $debug;

    /** @var callable */
    protected $output;

    /**
     * @param string $name
     * @param bool $handlesArrays
     * @param int $arity
     * @param array $block
     * @param Tokenery $tokenery
     * @throws InvalidArgumentException
     */
    public function __construct($name, $handlesArrays, $arity, array $block, Tokenery $tokenery)
    {
        $this->name = $name;
        if ($handlesArrays && !in_array($arity, [1, 2])) {
            throw new InvalidArgumentException('User function with ⪑ option has to have arity of 1 or 2');
        }
        $this->handlesArrays = $handlesArrays;
        $this->arity = $arity;
        $this->block = $block;
        parent::__construct($name, $tokenery);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $arguments = $this->fetchArguments($stack);
        $this->debug = $debug;
        $this->output = $output;
        $result = $this->run($arguments);
        $this->pushResult($stack, $result);
        $debug($this, $stack);
    }

    /**
     * @param AbstractValue[] $arg
     * @return array|AbstractToken
     */
    public function run($arg)
    {
        $run = function($a = null, $b = null) use ($arg) {
            $tokeneryLocal = clone $this->tokenery;

            if ($this->handlesArrays && $this->arity == 1) {
                $tokeneryLocal->setVariableForce('a', $a);
            } elseif ($this->handlesArrays && $this->arity == 2) {
                $tokeneryLocal->setVariableForce('a', $a);
                $tokeneryLocal->setVariableForce('b', $b);
            } else {
                $var = 'a';
                foreach ($arg as $argument) {
                    $tokeneryLocal->setVariableForce($var++, $argument->getValue());
                }
            }

            $this->setInternalTokenery($tokeneryLocal);

            $internalStack = new Stack();
            foreach($this->block as $token) {
                $token->execute($internalStack, $this->debug, $this->output);
            };

            return $internalStack->pop()->getValue();
        };

        if ($this->handlesArrays && $this->arity == 1) {
            return $this->handleScalarArrayOne($arg[0], $run);
        } elseif ($this->handlesArrays && $this->arity == 2) {
            return $this->handleScalarArrayTwo($arg[1], $arg[0], $run);
        }

        return $run();
    }

    public function getArity()
    {
        return $this->arity;
    }

    public function getDescription()
    {
        return '';
    }

    protected function getBlocks()
    {
        return ['block'];
    }

}
