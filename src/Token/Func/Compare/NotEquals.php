<?php
namespace Avris\QC\Token\Func\Compare;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class NotEquals extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return $a != $b ? 1 : 0; });
    }

    public function getDescription()
    {
        return '$a != $b';
    }
}
