<?php
namespace Avris\QC\Token\Func\Logic;

use Avris\QC\Token\Func\AbstractFunctionOne;

class LogicalNot extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return $a ? 0 : 1; });
    }

    public function getDescription()
    {
        return 'Not $a';
    }
}
