<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\ParseException;
use Avris\QC\Parser;
use Avris\QC\Stack;

class FunctionPromise extends AbstractFunction
{
    public function getArity()
    {
    }

    public function getDescription()
    {
    }

    public function run($arg)
    {
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $realFunction = $this->tokenery->buildToken(Parser::TOKEN_FUNCTION, $this->value);
        if ($realFunction instanceof FunctionPromise) {
            throw new ParseException(sprintf('Unrecognised function "%s"', $this->value));
        }
        return $realFunction->execute($stack, $debug, $output);
    }
}
