<?php
namespace Avris\QC\Token\Func;

class Sleep extends AbstractFunction
{
    protected function run($arg)
    {
        sleep($arg[0]->getValue());
    }

    public function getArity()
    {
        return 1;
    }

    public function pushesResult()
    {
        return false;
    }

    public function getDescription()
    {
        return 'Sleeps $a seconds';
    }
}
