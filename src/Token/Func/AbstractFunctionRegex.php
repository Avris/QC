<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\AbstractValue;
use Avris\QC\Token\Regex;

abstract class AbstractFunctionRegex extends AbstractFunctionTwo
{
    public function getArity()
    {
        return 2;
    }

    /**
     * @param AbstractValue[] $arg
     * @return array
     * @throws InvalidArgumentException
     */
    protected function handleArguments($arg)
    {
        $a = $arg[1]->getValue();
        $b = $arg[0]->getValue();

        if (($a instanceof Regex && $b instanceof Regex) || (!$a instanceof Regex && !$b instanceof Regex)) {
            throw new InvalidArgumentException('Exactly one argument of Regex related functions has to be a regex');
        }

        $regex = $a instanceof Regex ? $a : $b;
        $string = $a instanceof Regex ? $b : $a;

        return [$regex, $string];
    }
}
