<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Power extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return pow($a, $b); });
    }

    public function getDescription()
    {
        return '$a to the power of $b';
    }
}
