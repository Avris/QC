<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Minus extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return $a - $b; });
    }

    public function getDescription()
    {
        return '$a - $b';
    }
}
