<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class Trig extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        /** @var callable $function */
        $function = $arg[1]->getValue();

        if (!in_array($function, [
            'acos', 'acosh', 'asin', 'asinh', 'atan2', 'atan', 'atanh',
            'cos', 'cosh', 'sin', 'sinh', 'tan', 'tanh'
        ])) {
            throw new InvalidArgumentException(sprintf('"%s" is not a valid trigonometrical function', $function));
        }

        return $this->handleScalarArrayOne($arg[0], function($a) use ($function) {
            return $function($a);
        });
    }

    public function getDescription()
    {
        return 'Calculates $a trigonometrical function of value $b';
    }
}
