<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class Times extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1]->isString() && !$arg[0]->isString()) {
            return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return $a * $b; }, 1);
        }

        if (!$arg[1]->isString() && $arg[0]->isString()) {
            return str_repeat($arg[0]->getValue(), $arg[1]->getValue());
        }

        if ($arg[1]->isString() && !$arg[0]->isString()) {
            return str_repeat($arg[1]->getValue(), $arg[0]->getValue());
        }

        throw new InvalidArgumentException('Cannot multiply string by string');
    }

    public function getDescription()
    {
        return '$a * $b' . "\n" .
            'Or, if one argument is a string and the other is numeric, repeats the string given number of times';
    }
}
