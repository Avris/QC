<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class IsNumeric extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return is_numeric($a) ? 1 : 0; });
    }

    public function getDescription()
    {
        return 'Is $a numeric?';
    }
}
