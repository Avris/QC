<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Factorial extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], [$this, 'factorial']);
    }

    public function getDescription()
    {
        return '$a factorial';
    }

    public function factorial($a)
    {
        if ($a == 0) {
            return 1;
        }
        return $a * $this->factorial($a - 1);
    }
}
