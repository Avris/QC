<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Log extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return log($a, $b); });
    }

    public function getDescription()
    {
        return 'Logarithm of a $a with respect to base $b';
    }
}
