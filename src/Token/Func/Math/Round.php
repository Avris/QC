<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Round extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return round($a); });
    }

    public function getDescription()
    {
        return 'Round $a';
    }
}
