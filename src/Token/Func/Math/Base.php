<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunction;

class Base extends AbstractFunction
{
    protected function run($arg)
    {
        $fromBase = $arg[1]->getValue();
        $toBase = $arg[0]->getValue();
        return $this->handleScalarArrayOne($arg[2], function($a) use ($fromBase, $toBase) {
            return base_convert($a, $fromBase, $toBase);
        });
    }

    public function getDescription()
    {
        return 'Converts $a in base $b to base $c';
    }

    public function getArity()
    {
        return 3;
    }
}
