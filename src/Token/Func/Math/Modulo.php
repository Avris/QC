<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;

class Modulo extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) {
            if ($b == 0) {
                throw new InvalidArgumentException('Division by zero');
            }
            return is_float($a) || is_float($b) ? fmod($a, $b) : $a % $b;
        }, 1);
    }

    public function getDescription()
    {
        return '$a % $b';
    }
}
