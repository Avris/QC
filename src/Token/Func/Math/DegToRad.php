<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class DegToRad extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return deg2rad($a); });
    }

    public function getDescription()
    {
        return 'Convert $a degrees to radians';
    }
}
