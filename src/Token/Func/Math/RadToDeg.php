<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class RadToDeg extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return rad2deg($a); });
    }

    public function getDescription()
    {
        return 'Convert $a radians to degrees';
    }
}
