<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Hypotenuse extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return hypot($a, $b); });
    }

    public function getDescription()
    {
        return 'Hypotenuse of triangle with altitudes $a and $b';
    }
}
