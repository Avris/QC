<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Increment extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return $a + 1; });
    }

    public function getDescription()
    {
        return '$a++';
    }
}
