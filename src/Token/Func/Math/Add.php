<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Func\AbstractFunctionTwo;
use Avris\QC\Token\Variable;

class Add extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1] instanceof Variable) {
            throw new InvalidArgumentException('Argument 1 of ⩲ has to be a variable');
        }

        return $arg[1]->setValue($arg[1]->getValue() + $arg[0]->getValue());
    }

    public function getDescription()
    {
        return '$a += $b';
    }
}
