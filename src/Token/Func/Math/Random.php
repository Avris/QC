<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Random extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return mt_rand($a, $b); });
    }

    public function getDescription()
    {
        return 'Generates a random integer between $a and $b';
    }
}
