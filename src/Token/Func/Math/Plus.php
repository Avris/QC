<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionTwo;

class Plus extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if ($arg[1]->isString() || $arg[0]->isString()) {
            return $arg[1]->getValue() . $arg[0]->getValue();
        }

        return $this->handleScalarArrayTwo($arg[1], $arg[0], function($a, $b) { return $a + $b; });
    }

    public function getDescription()
    {
        return '$a + $b';
    }
}
