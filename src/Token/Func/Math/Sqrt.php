<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunctionOne;

class Sqrt extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $this->handleScalarArrayOne($arg[0], function($a) { return sqrt($a); });
    }

    public function getDescription()
    {
        return 'Square root of $a';
    }
}
