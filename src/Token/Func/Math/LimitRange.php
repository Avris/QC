<?php
namespace Avris\QC\Token\Func\Math;

use Avris\QC\Token\Func\AbstractFunction;

class LimitRange extends AbstractFunction
{
    protected function run($arg)
    {
        $min = $arg[1]->getValue();
        $max = $arg[0]->getValue();

        return $this->handleScalarArrayOne($arg[2], function($a) use ($min, $max) {
            if ($a < $min) { return $min; }
            if ($a > $max) { return $max; }
            return $a;
        });
    }

    public function getArity()
    {
        return 3;
    }

    public function getDescription()
    {
        return 'If $a is outside of range ($b, $c), it gets moved to its boundaries';
    }
}
