<?php
namespace Avris\QC\Token\Func;

class Pop extends AbstractFunctionOne
{
    protected function run($arg)
    {
    }

    public function pushesResult()
    {
        return false;
    }

    public function getDescription()
    {
        return 'Pops one element from the stack';
    }
}
