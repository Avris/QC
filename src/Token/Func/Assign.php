<?php
namespace Avris\QC\Token\Func;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Token\Variable;

class Assign extends AbstractFunctionTwo
{
    protected function run($arg)
    {
        if (!$arg[1] instanceof Variable) {
            throw new InvalidArgumentException('Left argument of "=" must be a variable');
        }

        $arg[1]->setValue($arg[0]->getValue());

        return $arg[1];
    }

    public function getDescription()
    {
        return '$a = $b';
    }
}
