<?php
namespace Avris\QC\Token\Func;

class Value extends AbstractFunctionOne
{
    protected function run($arg)
    {
        return $arg[0]->getValue();
    }

    public function getDescription()
    {
        return 'Returns the value of $a';
    }
}
