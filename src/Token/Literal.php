<?php
namespace Avris\QC\Token;

use Avris\QC\Exception\InvalidArgumentException;
use Avris\QC\Helper;
use Avris\QC\Parser;
use Avris\QC\Stack;

class Literal extends AbstractValue
{
    public function __construct($token, $value = null)
    {
        switch ($token) {
            case Parser::TOKEN_NULL:
                $realValue = null;
                break;
            case Parser::TOKEN_ARRAY:
                $realValue = $value;
                break;
            case Parser::TOKEN_NUMBER:
                $realValue = strpos($value, '.') === false ? (int)$value : (float)$value;
                break;
            case Parser::TOKEN_STRING:
                $realValue = str_replace('\\"', '"', $value);
                break;
            default:
                throw new InvalidArgumentException(sprintf('Invalid token %s', $token));
        }

        parent::__construct($token, $realValue);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $stack->push($this);
        $debug($this, $stack);
    }

    public function getDump()
    {
        return Helper::dumpValue($this->value);
    }
}
