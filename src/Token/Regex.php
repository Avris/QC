<?php
namespace Avris\QC\Token;

use Avris\QC\Parser;
use Avris\QC\Stack;

class Regex extends AbstractValue
{
    /** @var string */
    protected $pattern;

    /** @var string */
    protected $modifiers;

    /** @var bool */
    protected $replaceMode;

    /** @var string */
    protected $replacement;

    public function __construct($pattern, $modifiers = 'u', $replaceMode = false, $replacement = '')
    {
        $this->pattern = $pattern;
        $this->modifiers = $modifiers;
        $this->replaceMode = $replaceMode;
        $this->replacement = $replacement;

        parent::__construct(Parser::TOKEN_REGEX, $this);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $stack->push($this);
        $debug($this, $stack);
    }

    public function getDump()
    {
        return sprintf(
            '<%s:%s%s%s>',
            self::quote($this->pattern),
            $this->modifiers,
            $this->replaceMode ? ';' : '',
            $this->replaceMode ? self::quote($this->replacement) : ''
        );
    }

    public static function quote($string)
    {
        return str_replace([':', ';', '>'], ['\\:', '\\;', '\\>'], $string);
    }

    public static function unquote($string)
    {
        return str_replace(['\\:', '\\;', '\\>'], [':', ';', '>'], $string);
    }

    public function getPattern()
    {
        return sprintf(
            '#%s#%s',
            str_replace('#', '\\#', $this->pattern),
            $this->modifiers
        );
    }

    public function getReplacement()
    {
        return $this->replaceMode ? $this->replacement : '';
    }
}
