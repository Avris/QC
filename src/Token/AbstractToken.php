<?php
namespace Avris\QC\Token;

use Avris\QC\Stack;

abstract class AbstractToken
{
    /** @var string */
    protected $token;

    /** @var mixed */
    protected $value;

    /**
     * AbstractToken constructor.
     * @param $token
     * @param $value
     */
    public function __construct($token, $value = null)
    {
        $this->token = $token;
        $this->value = $value;
    }

    /**
     * @param Stack $stack
     * @param callable $debug
     * @param callable $output
     * @return
     */
    public abstract function execute(Stack $stack, callable $debug, callable $output);

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return AbstractToken
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function __toString()
    {
        return $this->getDump();
    }

    public function getDump()
    {
        return (string)$this->getValue();
    }
}
