<?php
namespace Avris\QC\Token;

abstract class AbstractValue extends AbstractToken
{
    public function __construct($token, $value)
    {
        parent::__construct($token, $value);
    }

    public function isString()
    {
        return is_string($this->value);
    }

    public function isNumber()
    {
        return is_int($this->value) || is_float($this->value);
    }

    public function isArray()
    {
        return is_array($this->value);
    }
}
