<?php
namespace Avris\QC\Token\Control;

use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Token\EmptyToken;
use Avris\QC\Tokenery;

class IfElseControl extends AbstractControl
{
    /** @var AbstractToken[] */
    protected $ifBlock;

    /** @var AbstractToken[] */
    protected $elseBlock;

    public function __construct(array $ifBlock, array $elseBlock, Tokenery $tokenery)
    {
        $this->ifBlock = $ifBlock;
        $this->elseBlock = $elseBlock;
        parent::__construct('IF', $tokenery);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $arguments = $this->fetchArguments($stack);
        $debug($this, $stack);
        $block = $arguments[0]->getValue() ? $this->ifBlock : $this->elseBlock;
        $internalStack = new Stack();
        foreach ($block as $internalToken) {
            $internalToken->execute($internalStack, $debug, $output);
        }
        $element = $internalStack->pop();
        if (!$element instanceof EmptyToken) {
            $stack->push($element);
        }
    }

    protected function getBlocks()
    {
        return ['ifBlock', 'elseBlock'];
    }
}
