<?php
namespace Avris\QC\Token\Control;

use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;

class WhileControl extends AbstractControl
{
    /** @var AbstractToken[] */
    protected $condition;

    /** @var AbstractToken[] */
    protected $block;

    public function __construct(array $condition, array $block, Tokenery $tokenery)
    {
        $this->condition = $condition;
        $this->block = $block;
        parent::__construct('WHILE', $tokenery);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        while (1) {
            $debug($this, $stack);

            $conditionStack = new Stack();
            foreach ($this->condition as $conditionToken) {
                $conditionToken->execute($conditionStack, $debug, $output);
            }
            if (!$conditionStack->pop()->getValue()) {
                break;
            }

            $internalStack = new Stack();
            foreach ($this->block as $internalToken) {
                $internalToken->execute($internalStack, $debug, $output);
            }
        }
    }

    protected function getBlocks()
    {
        return ['condition', 'block'];
    }
}
