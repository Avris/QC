<?php
namespace Avris\QC\Token\Control;

use Avris\QC\Stack;
use Avris\QC\Token\Func\QCFunction;
use Avris\QC\Tokenery;

class FunctionControl extends AbstractControl
{
    /** @var string */
    protected $name;

    /**
     * FunctionControl constructor.
     * @param string $name
     * @param bool $handlesArrays
     * @param int $arity
     * @param array $block
     * @param Tokenery $tokenery
     */
    public function __construct($name, $handlesArrays, $arity, array $block, Tokenery $tokenery)
    {
        $this->name = $name;
        parent::__construct('FUNC', $tokenery);
        $tokenery->registerInternalFunction(
            $name,
            new QCFunction($name, $handlesArrays, $arity, $block, $tokenery)
        );
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $debug($this, $stack);
    }

    protected function getBlocks()
    {
        return [];
    }
}
