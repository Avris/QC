<?php
namespace Avris\QC\Token\Control;

use Avris\QC\Stack;
use Avris\QC\Token\AbstractToken;
use Avris\QC\Tokenery;

class ForControl extends AbstractControl
{
    /** @var AbstractToken[] */
    protected $block;

    public function __construct(array $block, Tokenery $tokenery)
    {
        $this->block = $block;
        parent::__construct('FOR', $tokenery);
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $arguments = $this->fetchArguments($stack);

        $values = $arguments[0]->isArray() ? $arguments[0]->getValue() : range(1, $arguments[0]->getValue());

        $response = [];

        foreach ($values as $value) {
            $this->tokenery->setVariable('a', $value);

            $internalStack = new Stack();
            $debug($this, $internalStack);
            foreach ($this->block as $internalToken) {
                $internalToken->execute($internalStack, $debug, $output);
            }
            $response[] = $internalStack->pop()->getValue();
        }

        $this->pushResult($stack, $response);
    }

    protected function getBlocks()
    {
        return ['block'];
    }
}
