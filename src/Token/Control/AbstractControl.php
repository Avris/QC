<?php
namespace Avris\QC\Token\Control;

use Avris\QC\Parser;
use Avris\QC\Token\Func\AbstractFunctionOne;
use Avris\QC\Token\Variable;
use Avris\QC\Tokenery;

abstract class AbstractControl extends AbstractFunctionOne
{
    public function __construct($value, Tokenery $tokenery)
    {
        parent::__construct($value, $tokenery);
        $this->token = Parser::TOKEN_CONTROL;
    }

    public function run($arg)
    {
    }

    public function getDescription()
    {
        return '';
    }

    /**
     * @return array
     */
    protected abstract function getBlocks();

    public function setInternalTokenery(Tokenery $tokenery)
    {
        foreach ($this->getBlocks() as $blockName) {
            $newBlock = [];
            foreach ($this->{$blockName} as $token) {
                if ($token instanceof Variable) {
                    $token = $tokenery->getVariable($token->getName());
                } elseif ($token instanceof AbstractControl) {
                    $token->setInternalTokenery($tokenery);
                }
                $newBlock[] = $token;
            }

            $this->{$blockName} = $newBlock;
        }
    }
}
