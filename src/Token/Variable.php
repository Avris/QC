<?php
namespace Avris\QC\Token;

use Avris\QC\Helper;
use Avris\QC\Parser;
use Avris\QC\Stack;

class Variable extends AbstractValue implements \JsonSerializable
{
    /** @var string */
    protected $name;

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __construct($name, $value = null)
    {
        $this->name = $name;
        parent::__construct(Parser::TOKEN_VARIABLE, $value);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Variable
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function execute(Stack $stack, callable $debug, callable $output)
    {
        $stack->push($this);
        $debug($this, $stack);
    }

    function __toString()
    {
        return $this->name . '=' . Helper::dumpValue($this->value);
    }

    public function getDump()
    {
        return $this->name;
    }

    public function jsonSerialize()
    {
        return $this->value;
    }
}
